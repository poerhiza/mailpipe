# TODO

This is a list of features which would make this software better - create an issue if you start to or want to work on it.

## CI/CD

* Add more tests
* Add an analyzer https://gitlab.com/gitlab-org/security-products/analyzers/gosec/-/tree/master

## General

* Parse the body of emails via. rules