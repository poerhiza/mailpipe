#!/usr/bin/env bash
#
# Setup as a cronjob - run "vim /etc/crontab"
#
# Then Put the following in /etc/crontab: 
#
#  0  5  *  *  * root       /root/update_firewalld_blocklist.sh
#
# Then place this script at /root/update_firewalld_blocklist.sh
#
# This script will then run every day at 0500
#

/usr/bin/redis-cli --raw HKEYS sender_blocklist | egrep -v '127.0.0.1' > /tmp/blocklist
for ip in `cat /tmp/blocklist | sort | uniq`; do /usr/bin/firewall-cmd --permanent --add-rich-rule="rule family='ipv4' source address='$ip' reject"; echo "Blocked $ip"; done
rm -f /tmp/blocklist

/usr/bin/firewall-cmd --reload
