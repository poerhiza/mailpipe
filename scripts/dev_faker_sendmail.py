#!/usr/bin/env python3

import smtplib

from sys import argv

from faker import Faker

faker = Faker()

cnt = 1

use_default_to = False

if len(argv) > 1:
    cnt = int(argv[1])

if len(argv) > 2:
    use_default_to = "mailpipe@changeme.tld"

with smtplib.SMTP("172.16.242.11", 25) as smtp:
    smtp.helo(name="172.16.242.11")
    for _ in range(cnt):
        email_from = faker.email()
        email_to = (
            f"{faker.first_name()}@changeme.tld"
            if not use_default_to
            else use_default_to
        )
        message = f"""Subject: faker - {faker.sentence()}
From: {email_from}
To: {email_to}

{faker.paragraph(nb_sentences=5000)}
"""
        print(smtp.sendmail(email_from, [email_to], message))
