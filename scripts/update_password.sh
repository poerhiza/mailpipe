#!/bin/bash

EMAILS=`echo 'hkeys emails' | redis-cli -e --raw`
EMAILS_MENU=`echo -ne "${EMAILS}\ndone"`

PS3='Update account password #) '
emails_menu=($EMAILS_MENU)
emails=($EMAILS)
select primary in "${emails_menu[@]}"; do
    case $primary in
	"done")
	    echo "User requested exit"
	    exit
	    ;;
         *)
        echo "option $primary"
        if [[ " ${emails[*]} " =~ " ${primary} " ]]; then
          echo "Using $primary as your primary email to link all other accounts with"
          read -p "Enter the new password: " -s pw
          for e in $EMAILS; do
            if [[ "${primary}" != "${e}" ]]; then
              continue
            fi
            k=`echo "hget emails ${e}" | redis-cli`
            a=`echo "${pw}" | mailpipe-config -hash`
            echo "HMSET emails ${e} ${a}" | redis-cli -e --raw
          done
        else
          echo "Invalid option: ${REPLY}"
        fi
        ;;
    esac
done
