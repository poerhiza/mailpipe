#!/bin/sh

if [ ! -f /etc/mailpipe/api.json ]; then
    echo "Create a volume mapped to /etc/mailpipe and populate api.json"
    exit 1
fi

until nc -zv "redis" 6379; do
  >&2 echo "The Redis server is unavailable - sleeping"
  sleep 1
done

sleep 2

/usr/local/bin/mailpipe-api -v -config /etc/mailpipe/api.json