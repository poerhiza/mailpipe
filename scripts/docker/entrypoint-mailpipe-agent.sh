#!/bin/sh

set -e

if [ ! -f /etc/mailpipe/agent.json ]; then
    echo "Create a volume mapped to /etc/mailpipe and populate agent.json"
    exit 1
fi
  
until nc -zv "redis" 6379; do
  >&2 echo "The Redis server is unavailable - sleeping"
  sleep 1
done

sleep 2

/usr/local/bin/mailpipe -v -config /etc/mailpipe/agent.json