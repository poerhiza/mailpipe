#!/bin/bash

EMAILS=`echo 'hkeys emails' | redis-cli --raw`
EMAILS_MENU=`echo -ne "${EMAILS}\ndone"`

PS3='Link all accounts under email #) '
emails_menu=($EMAILS_MENU)
emails=($EMAILS)
select primary in "${emails_menu[@]}"; do
    case $primary in
	"done")
	    echo "User requested exit"
	    exit
	    ;;
         *)
        echo "option $primary"
        if [[ " ${emails[*]} " =~ " ${primary} " ]]; then
          echo "Using $primary as your primary email to link all other accounts with"
          for e in $EMAILS; do
            if [[ "${primary}" == "${e}" ]]; then
              continue
            fi
            k=`echo "hget emails ${e}" | redis-cli`
            a=`echo "${e}${k}" | mailpipe-config -hash | tr -d '\n' | cut -f3 -d:`
            echo "HMSET ${primary}|||account_links ${e} ${a}" | redis-cli --raw
          done
        else
          echo "Invalid option: ${REPLY}"
        fi
        ;;
    esac
done

