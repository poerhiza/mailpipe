#!/bin/bash
#
# Thanks: https://stackoverflow.com/questions/29161323/how-to-keep-associative-array-order
#

base=`pwd`

declare -A commands
commands["$base/../test"]='redis-cli monitor'
commands["$base/../test/"]='redis-cli'
commands["$base/../../mailpipe-gui/"]='npm run start'
commands["$base/.."]='MAILPIPE_API_CONFIG_PASSWORD=superduperpasswordmagic MAILPIPE_API_CONFIG_SALT=5f4dcc3b5aa765d61d8327deb882cf99 go run ./cmd/api/main.go -v -debug -config /etc/mailpipe/api.json'
commands["$base/../"]='sudo go run cmd/mailpipe/main.go -v -config /etc/mailpipe/mailbox.json'

declare -a order
order+=( "$base/../test" )
order+=( "$base/../test/" )
order+=( "$base/../../mailpipe-gui/" )
order+=( "$base/.." )
order+=( "$base/../" )

for d in "${order[@]}"; do
  cmd=${commands[$d]}
  gnome-terminal --working-directory="${d}" --tab --command "/usr/bin/bash -c \"echo '$cmd'; $cmd; exec bash\""
done

exit
