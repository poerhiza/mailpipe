#!/usr/bin/env bash

redis-cli --raw HKEYS sender_blocklist | egrep -v '127.0.0.1' > /tmp/blocklist
cat /etc/firewall.blocklist >> /tmp/blocklist
cat /tmp/blocklist | sort | uniq > /etc/firewall.blocklist
rm -f /tmp/blocklist

service ipfw restart
