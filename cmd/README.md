# mailpipe

To quickly run the application with out building it, run the following - which will print out the help information.

```go
go run ./cmd/mailpipe/main.go -h
```
