package main

import (
	"context"
	"flag"
	"fmt"
	"log"
	"os"
	"os/signal"
	"syscall"
	"time"

	pkg "gitlab.com/poerhiza/mailpipe/pkg"
	"gitlab.com/poerhiza/mailpipe/pkg/api"
	"gitlab.com/poerhiza/mailpipe/pkg/logger"
)

var config = "/etc/mailpipe/api.json"

// @title Mailpipe API
// @description This is the mailpipe API which serves the mailpipe-gui project

// @license.name GNU GENERAL PUBLIC LICENSE
// @license.url http://www.gnu.org/licenses/

// @host localhost:8443
// @BasePath /api/v1

func main() {
	verbose := flag.Bool("v", false, "Enable verbose output")
	version := flag.Bool("version", false, "Display the mailpipe version and exit")
	debug := flag.Bool("debug", false, "Enable debug output")

	flag.StringVar(&config, "config", config, "The config for Mailpipe API default looks in the current directory under ./data/configs/api.json")

	flag.Usage = usage
	flag.Parse()

	if *version {
		logger.Log(fmt.Sprintf("[I] Mailpipe API %s", pkg.BuildDate))
		os.Exit(0)
	}

	mailpipeAPIConfig, err := api.LoadAndParseConfig(config, *verbose, *debug)

	if err != nil {
		log.Fatal(err.Error())
	}

	mailpipeAPI, err := api.New(*mailpipeAPIConfig)

	if err != nil {
		log.Fatal(err.Error())
	}

	err = mailpipeAPI.Run()

	if err != nil {
		log.Fatal(err.Error())
	}

	quit := make(chan os.Signal, 1)

	signal.Notify(quit, syscall.SIGINT, syscall.SIGTERM)
	<-quit

	fmt.Println()
	logger.Log("[+] Shutting down the Mailpipe API")

	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)

	defer cancel()

	if err := mailpipeAPI.Shutdown(ctx); err != nil {
		logger.Log(fmt.Sprintf("[!] Server Shutdown Failure: %s", err.Error()))
	} else {
		logger.Log("[+] Server Shutdown Success")
	}

	shutdown()

	os.Exit(0)
}

func shutdown() {
	logger.Log("[+] Shutdown Mailpipe API")
}

func usage() {
	fmt.Println("Mailpipe API")
	flag.PrintDefaults()
}
