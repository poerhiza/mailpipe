package main

import (
	"bufio"
	"flag"
	"fmt"
	"io/ioutil"
	"net"
	"net/http"
	netMail "net/mail"
	"net/smtp"
	"os"
	"path"
	"regexp"
	"strings"

	"github.com/fatih/color"
	"github.com/jhillyerd/enmime"

	pkg "gitlab.com/poerhiza/mailpipe/pkg"
	"gitlab.com/poerhiza/mailpipe/pkg/logger"
)

// Created so that multiple inputs can be accecpted
type attachmentsFlag []string

func (i *attachmentsFlag) String() string {
	// change this, this is just can example to satisfy the interface
	return "my string representation"
}

func (i *attachmentsFlag) Set(value string) error {
	*i = append(*i, strings.TrimSpace(value))
	return nil
}

func main() {

	var attachments attachmentsFlag

	version := flag.Bool("version", false, "Display the mailpipe version and exit")
	force := flag.Bool("force", false, "Don't validate a single thing")

	from := flag.String("from", "", "The source E-Mail address")
	to := flag.String("to", "", "The destination E-Mail address")
	subject := flag.String("subject", "", "The E-Mail subject")
	body := flag.String("body", "", "The E-Mail body")
	server := flag.String("server", "127.0.0.1", "The destination E-Mail server hostname or IP address")
	port := flag.Int("port", 25, "The destination E-Mail server port")

	flag.Var(&attachments, "attach", "Attach file('s) to the E-Mail")

	flag.Usage = usage
	flag.Parse()

	if *version {
		color.Blue(fmt.Sprintf("Mailpipe Pipemail %s", pkg.BuildDate))
		os.Exit(0)
	}

	if *from == "" {
		logger.Log("[!] From can not be empty")
		usage()
	}

	if *to == "" {
		logger.Log("[!] To can not be empty")
		usage()
	}

	if *subject == "" {
		logger.Log("[!] Subject can not be empty")
		usage()
	}

	if *body == "" {
		stat, _ := os.Stdin.Stat()

		if (stat.Mode() & os.ModeCharDevice) == 0 {
			r := bufio.NewReader(os.Stdin)
			data, _ := ioutil.ReadAll(r)
			*body = string(data)
		} else {
			logger.Log("[!] Body can not be empty")
			usage()
		}
	}

	if *server == "" {
		logger.Log("[!] Server can not be empty")
		usage()
	}

	if *port == 0 {
		logger.Log("[!] Port can not be zero")
		usage()
	}

	if err := sendmail(*server, *port, *from, *to, *subject, *body, attachments, *force); err != nil {
		logger.Log(fmt.Sprintf("[!] Failed to send E-Mail: %s\n", err.Error()))
	}
}

func usage() {
	fmt.Printf("Pipemail Options\r\n")
	flag.PrintDefaults()
	os.Exit(0)
}

var emailRegex = regexp.MustCompile("^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$")

// IsEmailValid checks if the email provided passes the required structure - thanks goes to https://golangnews.org/2020/06/validating-an-email-address/
func IsEmailValid(e string) bool {

	if l := len(e); l < 3 || l > 254 {
		return false
	}

	if !emailRegex.MatchString(e) {
		return false
	}

	parts := strings.Split(e, "@")

	if len(parts) > 1 {
		subparts := strings.Split(parts[1], ".")

		if len(subparts) > 1 {
			mx, err := net.LookupMX(parts[1])

			if err != nil || len(mx) == 0 {
				return false
			}
		} else if parts[1] == "localhost" || parts[1] == "127.0.0.1" {
			return true
		} else {
			return false
		}
	} else {
		return false
	}

	return true
}

func sendmail(smtpServer string, smtpPort int, from string, to string, subject string, body string, attachments []string, force bool) error {

	if !force && !IsEmailValid(to) {
		return fmt.Errorf("invalid to address")
	}

	if !force && !IsEmailValid(from) {
		return fmt.Errorf("invalid from address")
	}

	smtpUsername := os.Getenv("PIPEMAIL_USERNAME")
	smtpPassword := os.Getenv("PIPEMAIL_PASSWORD")

	smtpHost := fmt.Sprintf("%s:%d", smtpServer, smtpPort)

	var smtpAuth smtp.Auth

	if smtpUsername != "" {
		smtpAuth = smtp.PlainAuth("", smtpUsername, smtpPassword, smtpServer)
	} else {
		smtpAuth = nil
	}

	sender := enmime.NewSMTP(smtpHost, smtpAuth)

	var tos []netMail.Address
	tos = append(tos, netMail.Address{
		Name:    "<PIPEMAIL_TO>",
		Address: to,
	})

	master := enmime.Builder().
		From("<PIPEMAIL_FROM>", from).
		Subject(subject).
		Text([]byte(body)).
		ToAddrs(tos).
		ReplyTo("<PIPEMAIL_FROM>", from)

	var ret *error

	for _, attachment := range attachments {

		bytes, err := ioutil.ReadFile(attachment) // #nosec G304

		if err != nil {
			ret = &err
			break
		}

		mimeType := http.DetectContentType(bytes)

		master = master.AddAttachment(
			bytes,
			mimeType,
			path.Base(attachment),
		)
	}

	if ret != nil {
		return *ret
	}

	if master.Error() != nil {
		return master.Error()
	}

	err := master.SendWithReversePath(sender, from)

	if err != nil {
		logger.Log(fmt.Sprintf("[!] E-Mail failed to send: %v\n", master))
		return err
	}

	logger.Log("[+] E-Mail sent!")

	return nil
}
