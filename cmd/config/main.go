package main

import (
	"bufio"
	"flag"
	"fmt"
	"log"
	"os"
	"strings"

	"github.com/fatih/color"
	"golang.org/x/crypto/bcrypt"

	pkg "gitlab.com/poerhiza/mailpipe/pkg"
	"gitlab.com/poerhiza/mailpipe/pkg/storage/crypto"
)

func main() {

	version := flag.Bool("version", false, "Display the mailpipe version and exit")

	salt := flag.Bool("salt", false, "Use salt when hashing a value")
	hash := flag.Bool("hash", false, "Generate a hashed password for use in the config")
	hashsum := flag.Bool("hashsum", false, "Generate a hash from an input")
	encrypt := flag.Bool("encrypt", false, "Generate an encrypted field for use in the config")
	decrypt := flag.Bool("decrypt", false, "Generate an decrypted field to validate an entry in your config")
	gencrypto := flag.Bool("gencrypto", false, "Generate a CA, Private key, and Public key")

	flag.Usage = usage
	flag.Parse()

	if *version {
		color.Blue(fmt.Sprintf("Mailpipe Config %s", pkg.BuildDate))
		os.Exit(0)
	}

	if *hash {
		var saltString string
		var input string

		if *salt {
			fmt.Fprint(os.Stderr, "\nSalt:")

			_, err := fmt.Scanln(&saltString)

			if err != nil {
				log.Fatal(err)
			}
		}

		fmt.Fprint(os.Stderr, "\nData:")

		_, err := fmt.Scanln(&input)

		if err != nil {
			fmt.Println(err.Error())
			input = ""
		}

		var hashedInput string

		if *salt {
			hashedInput = crypto.HashPasswordWithSalt(input, saltString)
		} else {
			hashedInput = crypto.HashString(input)
		}

		if *hashsum {
			fmt.Printf("%s", hashedInput)
		} else {
			hash, err := bcrypt.GenerateFromPassword([]byte(hashedInput[0:72]), bcrypt.DefaultCost)

			if err != nil {
				fmt.Println(err.Error())
				os.Exit(1)
			}

			fmt.Printf("%s", hash)
		}

		os.Exit(0)
	}

	if *encrypt {
		var saltString string
		var input string
		var key string

		fmt.Print("\nAD (Salt):")

		_, err := fmt.Scanln(&saltString)

		if err != nil {
			log.Fatal(err)
		}

		reader := bufio.NewReader(os.Stdin)

		fmt.Print("\nKey (password):")

		// ReadString will block until the delimiter is entered
		key, err = reader.ReadString('\n')

		if err != nil {
			fmt.Println("An error occured while reading input. Please try again", err)
			return
		}

		// remove the delimeter from the string
		key = strings.TrimSuffix(key, "\n")

		fmt.Print("\nInput:")

		// ReadString will block until the delimiter is entered
		input, err = reader.ReadString('\n')

		if err != nil {
			fmt.Println("An error occured while reading input. Please try again", err)
			return
		}

		// remove the delimeter from the string
		input = strings.TrimSuffix(input, "\n")

		var data string

		data, err = crypto.AESGCMEncrypt(key, input, []byte(saltString), true)

		if err != nil {
			fmt.Println(err.Error())
			input = ""
		}

		fmt.Printf("\nEncrypted:\n%s\n\n", data)

		os.Exit(0)
	}

	if *decrypt {
		var saltString string
		var input string
		var key string

		fmt.Print("\nAD (Salt):")

		_, err := fmt.Scanln(&saltString)

		if err != nil {
			log.Fatal(err)
		}

		reader := bufio.NewReader(os.Stdin)

		fmt.Print("\nKey (password):")

		// ReadString will block until the delimiter is entered
		key, err = reader.ReadString('\n')

		if err != nil {
			fmt.Println("An error occured while reading input. Please try again", err)
			return
		}

		// remove the delimeter from the string
		key = strings.TrimSuffix(key, "\n")

		fmt.Print("\nInput:")

		// ReadString will block until the delimiter is entered
		input, err = reader.ReadString('\n')

		if err != nil {
			fmt.Println("An error occured while reading input. Please try again", err)
			return
		}

		// remove the delimeter from the string
		input = strings.TrimSuffix(input, "\n")

		var data string

		data, err = crypto.AESGCMDecrypt(key, input, []byte(saltString), true)

		if err != nil {
			fmt.Println(err.Error())
			input = ""
		}

		fmt.Printf("\nDecrypted:\n%s\n\n", data)

		os.Exit(0)
	}

	if *gencrypto {
		caBytes, publicKey, privateKey, err := crypto.GenerateCertificate()

		if err != nil {
			log.Fatal(err)
		}

		err = crypto.WriteFile("./authority.pem", *caBytes)

		if err != nil {
			log.Fatal(err)
		}

		err = crypto.WriteFile("./private.pem", *publicKey)

		if err != nil {
			log.Fatal(err)
		}

		err = crypto.WriteFile("./public.pem", *privateKey)

		if err != nil {
			log.Fatal(err)
		}

		os.Exit(0)
	}

	usage()
}

func usage() {
	fmt.Printf("Mailpipe Config\r\n")
	flag.PrintDefaults()
}
