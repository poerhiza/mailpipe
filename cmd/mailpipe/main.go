package main

import (
	"flag"
	"fmt"
	"log"
	"os"
	"os/signal"
	"syscall"
	"time"

	"github.com/fatih/color"

	pkg "gitlab.com/poerhiza/mailpipe/pkg"
	"gitlab.com/poerhiza/mailpipe/pkg/logger"
	"gitlab.com/poerhiza/mailpipe/pkg/mailpipe"
)

var config = "/etc/mailpipe/mailbox.json"

func main() {
	verbose := flag.Bool("v", false, "Enable verbose output")
	version := flag.Bool("version", false, "Display the mailpipe version and exit")
	debug := flag.Bool("debug", false, "Enable debug output")

	flag.StringVar(&config, "config", config, "The config for Mailpipe mailboxes default looks in the current directory under ./data/configs/mailbox.json")

	flag.Usage = usage
	flag.Parse()

	if *version {
		color.Blue(fmt.Sprintf("Mailpipe %s", pkg.BuildDate))
		os.Exit(0)
	}

	mailpipeConfig, err := mailpipe.LoadAndParseConfig(config, *verbose, *debug)

	if err != nil {
		logger.Log(err.Error())
		os.Exit(1)
	}

	mp, err := mailpipe.New(mailpipeConfig)

	if err != nil {
		log.Fatal(err.Error())
	}

	err = mp.Run()

	if err != nil {
		log.Fatal(err.Error())
	}

	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt, syscall.SIGTERM)
	go func() {
		<-c

		err := mp.Shutdown()
		if err != nil {
			log.Fatal(err.Error())
		}

		shutdown()
		os.Exit(0)
	}()

	for {
		time.Sleep(time.Minute)
	}
}

func shutdown() {
	fmt.Println("shutdown")
}

func usage() {
	fmt.Printf("Mailpipe\r\n")
	flag.PrintDefaults()
}
