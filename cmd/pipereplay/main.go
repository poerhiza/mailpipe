package main

import (
	"flag"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"path"
	"strings"

	netMail "net/mail"

	"github.com/fatih/color"
	"github.com/jhillyerd/enmime"

	pkg "gitlab.com/poerhiza/mailpipe/pkg"
	"gitlab.com/poerhiza/mailpipe/pkg/logger"
)

// Created so that multiple inputs can be accecpted
type attachmentsFlag []string

func (i *attachmentsFlag) String() string {
	// change this, this is just can example to satisfy the interface
	return "my string representation"
}

func (i *attachmentsFlag) Set(value string) error {
	*i = append(*i, strings.TrimSpace(value))
	return nil
}

func main() {

	var attachments attachmentsFlag

	version := flag.Bool("version", false, "Display the mailpipe version and exit")
	server := flag.String("server", "127.0.0.1", "The destination E-Mail server hostname or IP address")
	port := flag.Int("port", 25, "The destination E-Mail server port")

	flag.Var(&attachments, "attach", "Attach file('s) to the E-Mail")

	flag.Usage = usage
	flag.Parse()

	if *version {
		color.Blue(fmt.Sprintf("Mailpipe Pipemail %s", pkg.BuildDate))
		os.Exit(0)
	}

	if *server == "" {
		logger.Log("[!] Server can not be empty")
		usage()
	}

	if *port == 0 {
		logger.Log("[!] Port can not be zero")
		usage()
	}

	// Parse message body with enmime.
	env, err := enmime.ReadEnvelope(os.Stdin)
	if err != nil {
		fmt.Print(err)
		return
	}

	if err != nil {
		logger.Log(fmt.Sprintf("[!] Failed to parse input: %s\n", err.Error()))
	}
	var from = "tester@mailpipe.tld"

	var tos []netMail.Address
	tos = append(tos, netMail.Address{
		Name:    "<PIPEMAIL_TO>",
		Address: "mailpipe@changeme.tld",
	})

	froms, _ := env.AddressList("From")

	master := enmime.Builder().
		From(froms[0].Name, froms[0].Address).
		Subject(env.GetHeader("Subject")).
		Text([]byte(env.Text)).
		HTML([]byte(env.HTML)).
		ToAddrs(tos).
		ReplyTo("<PIPEMAIL_FROM>", from)

	for _, k := range env.GetHeaderKeys() {
		if strings.ToLower(k) != "to" {
			master.Header(k, env.GetHeader(k))
		}
	}

	for _, inline := range env.Inlines {
		master.AddInline(inline.Content, inline.ContentType, inline.FileName, inline.ContentID)
	}

	var ret *error

	for _, attachment := range attachments {

		bytes, err := ioutil.ReadFile(attachment) // #nosec G304

		if err != nil {
			ret = &err
			break
		}

		mimeType := http.DetectContentType(bytes)

		master = master.AddAttachment(
			bytes,
			mimeType,
			path.Base(attachment),
		)
	}

	if ret != nil {
		logger.Log(fmt.Sprintf("[!] Failed to parse input: %s\n", err.Error()))
		return
	}

	if master.Error() != nil {
		logger.Log(fmt.Sprintf("[!] Failed to parse input: %s\n", master.Error()))
		return
	}

	sender := enmime.NewSMTP(fmt.Sprintf("%s:%d", *server, *port), nil)

	err = master.SendWithReversePath(sender, from)

	if err != nil {
		logger.Log(fmt.Sprintf("[!] E-Mail failed to send: %v\n", master))
		logger.Log(fmt.Sprintf("[!] Failed to parse input: %s\n", err.Error()))
		return
	}

	logger.Log("[+] E-Mail sent!")

}

func usage() {
	fmt.Printf("Pipemail Options\r\n")
	flag.PrintDefaults()
	os.Exit(0)
}
