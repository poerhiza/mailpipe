package redis

import (
	"errors"
	"fmt"
	"strconv"
	"time"

	"github.com/mediocregopher/radix/v3"

	config "gitlab.com/poerhiza/mailpipe/pkg/config/transport"
	"gitlab.com/poerhiza/mailpipe/pkg/constants"
	"gitlab.com/poerhiza/mailpipe/pkg/logger"
)

// Pool a public pool of redis clients
var Pool *radix.Pool = nil

// CreateMetadataEntry will create the metadata key entry as a hashset in redis
func CreateMetadataEntry(metadataKey string) error {
	return Do(radix.Cmd(nil, "HSET", metadataKey, "read", "false"))
}

// GetMetadataEntry will return the metadata key entry as a hashset in redis
func GetMetadataEntry(metadataKey string) (map[string]string, error) {
	var metadata map[string]string

	err := Do(radix.Cmd(&metadata, "HGETALL", metadataKey))

	return metadata, err
}

// AddAddressToBlocklist will create an entry within the blocklist which is a hashset in redis
func AddAddressToBlocklist(blocklistKey string, entryKey string, entryValue string) error {
	return Do(radix.Cmd(nil, "HSET", blocklistKey, entryKey, entryValue))
}

// AddAddressToBlocklistCount will add and or increment the number of times this address has sent 'bad mail'
func AddAddressToBlocklistCount(blocklistCountKey string, addressKey string) error {
	return Do(radix.Cmd(nil, "ZINCRBY", blocklistCountKey, "1", addressKey))
}

// RemoveMetadataEntry deletes all keys which match the metadatahashkey
func RemoveMetadataEntry(metadataHaskKey string) error {
	var keys []string

	err := Do(radix.Cmd(&keys, "KEYS", fmt.Sprintf("*%s*", metadataHaskKey)))

	if err != nil {
		return err
	}

	for _, key := range keys {
		err = Do(radix.Cmd(nil, "DEL", key))

		if err != nil {
			return err
		}
	}

	return nil
}

// SetExpirationOnMetadataEntry will set a time from now when an email entry will be deleted by redis
func SetExpirationOnMetadataEntry(metadataKey string, duration time.Duration) error {
	unixtimestamp := strconv.FormatInt(time.Now().Add(duration).Unix(), 10)
	return Do(radix.Cmd(nil, "EXPIREAT", metadataKey, unixtimestamp))
}

// AddMetadataToEntry will append k/v to the metadata key
func AddMetadataToEntry(metadataHashKey string, key string, value string) error {
	return Do(radix.Cmd(nil, "HSET", metadataHashKey, key, value))
}

// AddAttachmentEntry will create and/or add to a redis set the filehash and filepath of an attachment
func AddAttachmentEntry(metadataHashKey string, attachmentHash string, filename string, filepath string) error {
	return Do(radix.Cmd(nil, "SADD", constants.GetAttachmentKey(metadataHashKey, attachmentHash, filename), filepath))
}

// GetAttachments will return the attachment found at the key
func GetAttachments(metadataHashKey string, attachmentHashKey string) ([]string, error) {
	var attachments []string

	err := Do(radix.Cmd(&attachments, "KEYS", constants.GetAttachmentKey(metadataHashKey, attachmentHashKey, "*")))

	return attachments, err
}

// SetAccountPassword will take an email and set it's password in the email database
func SetAccountPassword(email string, password string) error {
	return Do(radix.Cmd(nil, "HSET", constants.EmailDatabaseName, email, password))
}

// GetAccountPassword will take an email and get it's password from the email database
func GetAccountPassword(email string) (string, error) {
	var password string
	err := Do(radix.Cmd(&password, "hget", constants.EmailDatabaseName, email))
	return password, err
}

// GetLinkedAccounts returns the accounts which are linked to the provided email
func GetLinkedAccounts(email string) (map[string]string, error) {
	var accounts map[string]string

	err := Do(radix.Cmd(&accounts, "HGETALL", constants.AccountLink(email)))

	return accounts, err
}

// RemoveAccountLink will remove a link between two accounts
func RemoveAccountLink(requestorEmail string, approverEmail string) error {
	err := Do(radix.Cmd(nil, "HDEL", constants.AccountLinkRequest(requestorEmail), approverEmail))

	if err != nil {
		return err
	}

	return Do(radix.Cmd(nil, "HDEL", constants.AccountLink(approverEmail), requestorEmail))
}

// GetAccountUnreadCount will return the cardinality of the unread box
func GetAccountUnreadCount(email string) (int, error) {
	var count = 0
	err := Do(radix.Cmd(&count, "ZCARD", constants.GetBoxKey(email, constants.UnreadBoxName)))

	if err != nil {
		return count, err
	}

	return count, nil
}

// LinkRequestAccept from a user for a handle@domain.tld -> handle@domain.tld
func LinkRequestAccept(requestorEmail string, approverEmail string, approverEmailHash string) (bool, error) {
	err := Do(radix.Cmd(nil, "HSET", constants.AccountLink(requestorEmail), approverEmail, approverEmailHash))

	if err != nil {
		return false, err
	}

	return SetLinkRequest(requestorEmail, approverEmail, "accepted")
}

// LinkRequestDeny from a user for a handle@domain.tld -> handle@domain.tld
func LinkRequestDeny(requestorEmail string, approverEmail string) (bool, error) {
	err := Do(radix.Cmd(nil, "HDEL", constants.AccountLink(requestorEmail), approverEmail))

	if err != nil {
		return false, err
	}

	return SetLinkRequest(requestorEmail, approverEmail, "denied")
}

// SetLinkRequest from a user for a handle@domain.tld -> handle@domain.tld
func SetLinkRequest(requestorEmail string, approverEmail string, status string) (bool, error) {
	ret := true

	err := Do(radix.Cmd(nil, "HSET", constants.AccountLinkRequest(approverEmail), requestorEmail, status))

	if err != nil {
		logger.Log(err.Error())
		ret = false
	}

	return ret, nil
}

// GetLinkRequests returns the account link requests for this link
func GetLinkRequests(email string) (map[string]string, error) {
	var requests map[string]string

	err := Do(radix.Cmd(&requests, "HGETALL", constants.AccountLinkRequest(email)))

	return requests, err
}

// AddBoxEntry will create and/or add to a redis sorted set for a specified box
func AddBoxEntry(boxKey string, score string, metadataKey string) error {
	return Do(radix.Cmd(nil, "ZADD", boxKey, score, metadataKey))
}

// GetBoxEntry returns the account link requests for this link
func GetBoxEntryByID(boxKey string, messageID string) (map[string]map[string]string, error) {
	var boxKeys map[string]map[string]string
	// TODO: make sure we don't leak cursors...?

	err := Do(radix.Cmd(&boxKeys, "ZSCAN", boxKey, "0", "match", fmt.Sprintf("*%s*", messageID)))

	if err != nil {
		return boxKeys, err
	}

	return boxKeys, nil
}

// RemoveBoxEntry will remove an entry from the supplied box
func RemoveBoxEntry(boxKey string, messageKey string) error {
	return Do(radix.Cmd(nil, "ZREM", boxKey, messageKey))
}

// GetBoxEntry will retrieve an entry from the box
func GetBoxEntry(boxKey string) (map[string]string, error) {
	var emailItem map[string]string
	err := Do(radix.Cmd(&emailItem, "HGETALL", boxKey))

	return emailItem, err
}

// GetBoxEntry will retrieve an entry from the box
func GetBoxEntryKey(boxKey string, emailKey string) (string, error) {
	var emailItem string
	err := Do(radix.Cmd(&emailItem, "HGET", boxKey, emailKey))

	return emailItem, err
}

// GetBoxListing retrieves all the entries in a box
func GetBoxListing(email string, box string) ([]string, error) {

	var boxMapping []string

	err := Do(radix.Cmd(&boxMapping, "ZRANGE", constants.GetBoxKey(email, box), "0", "-1"))

	return boxMapping, err
}

// GetBoxListingByRange retrieves all the entries in a box by range
func GetBoxListingByRange(email string, box string, start_date string, end_date string) ([]string, error) {

	var boxMapping []string

	err := Do(radix.Cmd(&boxMapping, "ZRANGEBYSCORE", constants.GetBoxKey(email, box), start_date, end_date))

	return boxMapping, err
}

// GetEveryBox a listing of all boxes for an email
func GetEveryBox(email string) ([]string, error) {
	var boxes []string

	err := Do(radix.Cmd(&boxes, "KEYS", constants.GetBoxKey(email, "*")))

	return boxes, err
}

// GetSiteWideTransports returns any site wide transports configured for the server
func GetSiteWideTransports() (map[string]string, error) {
	var transports map[string]string

	err := Do(radix.Cmd(&transports, "HGETALL", constants.GetTransportKey(config.TransportConfigStatusSiteWide)))

	return transports, err
}

// AddSiteWideTransport from a transport configuration item, populate the server wide transport hashes
func AddSiteWideTransport(title string, transportConfig string) error {
	return Do(radix.Cmd(nil, "HSET", constants.GetTransportKey(config.TransportConfigStatusSiteWide), constants.GetTransportTitle(title, config.TransportConfigStatusSiteWide), transportConfig))
}

// AddUserTransport from a transport configuration item, populate a user transport hashes
func AddUserTransport(email string, title string, transportConfig string) error {
	return Do(radix.Cmd(nil, "HSET", constants.GetTransportKey(email), constants.GetTransportTitle(title, config.TransportConfigStatusAccount), transportConfig))
}

// GetUserTransports returns any site wide transports configured for the server
func GetUserTransports(email string) (map[string]string, error) {
	var transports map[string]string

	err := Do(radix.Cmd(&transports, "HGETALL", constants.GetTransportKey(email)))

	return transports, err
}

// RemoveUserTransport will remove a transport based upon id for an email
func RemoveUserTransport(email string, title string) error {
	return Do(radix.Cmd(nil, "HDEL", constants.GetTransportKey(email), constants.GetTransportTitle(title, config.TransportConfigStatusAccount)))
}

// GetHaskKeys will remove a transport based upon id for an email
func GetHashKeys(key string) ([]string, error) {
	var metadata []string

	err := Do(radix.Cmd(&metadata, "HKEYS", key))

	return metadata, err
}

// GetUserContacts return all contacts for a user
func GetUserContacts(email string) (map[string]string, error) {
	var contacts map[string]string

	err := Do(radix.Cmd(&contacts, "HGETALL", constants.GetContactsKey(email)))

	return contacts, err
}

// ScanHashSet will retrieve an entry from a hashset
func ScanHashSet(key string, search string, max int) (map[string]string, error) {
	matches := make(map[string]string)

	if Pool == nil {
		return matches, errors.New("redis pool not created yet")
	}

	scanner := radix.NewScanner(Pool, radix.ScanOpts{Command: "HSCAN", Key: key, Pattern: search, Count: max})

	var k string
	var c int = 1
	var nextItemKey string

	for scanner.Next(&k) {
		if c%2 != 0 {
			nextItemKey = k
		} else {
			matches[nextItemKey] = k
		}
		c++
	}

	return matches, scanner.Close()
}

// Do an action in the redis pool
func Do(a radix.Action) error {

	if Pool != nil {
		return Pool.Do(a)
	}

	return fmt.Errorf("no pool found, try calling New")
}

// New creates a redis pool for use
func New(redisConfig Config) (*radix.Pool, error) {

	if Pool != nil {
		return Pool, nil
	}

	rp, err := radix.NewPool(redisConfig.Protocol, redisConfig.Address, redisConfig.Poolsize)

	if err != nil {
		return nil, err
	}

	if rp == nil {
		return nil, fmt.Errorf("unable to create redis pool")
	}

	Pool = rp

	return Pool, nil
}
