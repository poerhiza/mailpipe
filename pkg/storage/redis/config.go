package redis

// Config for redis connections
type Config struct {
	Protocol string `json:"protocol"`
	Address  string `json:"address"`
	Poolsize int    `json:"poolsize"`
}
