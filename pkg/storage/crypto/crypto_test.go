package crypto

import (
	"os"
	"testing"
)

func TestMain(m *testing.M) {
	os.Exit(m.Run())
}

func TestShouldHashStringWithMD5(t *testing.T) {
	hash := MD5HashString("string")

	if hash != "b45cffe084dd3d20d928bee85e7b0f21" {
		t.Errorf("This is not the hash of 'string' in when using MD5: %s", hash)
	}
}

func TestShouldHashStringWithSHA3(t *testing.T) {
	hash := HashString("string")

	if hash != "f18e2bcbdba6857273b689ec789ab616699a858e95b05b0360fb340e9a63ec3a883ade4bb18c929259dbf661f25a5c7223915c5bab39e0b81e3cb4068f074463" {
		t.Errorf("This is not the hash of 'string' in when using SHA3: %s", hash)
	}
}

func TestShouldHashPasswordWithSalt(t *testing.T) {
	saltedHash := HashPasswordWithSalt("string", "salt")

	if saltedHash != "b15a7d78cb9da4a8ac0939bbc870c203ac8de1207abf8f888f438b2337a8003c68550a6de867653cdc79c61fef4d0f1831db0198816795a4ef398e26a0ac4cba" {
		t.Errorf("This is not the hash of 'string' in when using SHA3 with our salt of 'salt': %s", saltedHash)
	}
}
