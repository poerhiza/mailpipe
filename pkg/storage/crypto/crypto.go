package crypto

import (
	"bytes"
	"crypto/aes"
	"crypto/cipher"
	"crypto/md5" // #nosec G501
	"crypto/rand"
	"crypto/rsa"
	"crypto/x509"
	"crypto/x509/pkix"
	"encoding/hex"
	"encoding/pem"
	"fmt"
	"io"
	"math/big"
	"net"
	"os"
	"strings"
	"time"

	"golang.org/x/crypto/sha3"
)

// AESGCMEncrypt is used to AES-256 encrypt a piece of data
func AESGCMEncrypt(key string, pt string, ad []byte, hashkey bool) (string, error) {
	if hashkey {
		key = MD5HashString(key)
	}
	block, err := aes.NewCipher([]byte(key))
	if err != nil {
		return "", err
	}
	aesgcm, err := cipher.NewGCM(block)

	if err != nil {
		return "", err
	}

	nonce := make([]byte, aesgcm.NonceSize())

	if _, err = io.ReadFull(rand.Reader, nonce); err != nil {
		fmt.Println(err)
	}

	return fmt.Sprintf("%s:%s", hex.EncodeToString(nonce), hex.EncodeToString(aesgcm.Seal(nil, nonce, []byte(pt), ad))), nil
}

// AESGCMDecrypt is used to AES-256 decrypt a piece of data
func AESGCMDecrypt(key string, encodedCipherText string, ad []byte, hashkey bool) (string, error) {
	data := strings.Split(encodedCipherText, ":")

	if len(data) < 1 {
		return "", fmt.Errorf("%s", "encoded_ct is the wrong foramt")
	}

	nonce, _ := hex.DecodeString(data[0])
	ct, _ := hex.DecodeString(data[1])

	if hashkey {
		key = MD5HashString(key)
	}

	block, err := aes.NewCipher([]byte(key))

	if err != nil {
		return "", err
	}
	aesgcm, err := cipher.NewGCM(block)

	if err != nil {
		return "", err
	}

	plaintext, err := aesgcm.Open(nil, nonce, ct, ad)

	if err != nil {
		return "", err
	}

	return string(plaintext[:]), nil
}

// HashPasswordWithSalt take a password from the user and the salt and return a hashed version
func HashPasswordWithSalt(password string, salt string) string {
	return HashString(fmt.Sprintf("%s%s", password, salt))
}

// HashString will hash the supplied string
func HashString(str string) string {
	hash := sha3.New512()
	hash.Write([]byte(str))
	return fmt.Sprintf("%x", hash.Sum(nil))
}

// MD5HashString will hash the supplied string
func MD5HashString(str string) string {
	return fmt.Sprintf("%x", md5.Sum([]byte(str))) /* #nosec G401 */
}

// GenerateCertificate create a certifcate authority, public key and private key - returning the bytes for each or an error
func GenerateCertificate() (*[]byte, *[]byte, *[]byte, error) {
	// set up our CA certificate
	ca := &x509.Certificate{
		SerialNumber: big.NewInt(2019),
		Subject: pkix.Name{
			Organization:  []string{"Company, INC."},
			Country:       []string{"US"},
			Province:      []string{""},
			Locality:      []string{"San Francisco"},
			StreetAddress: []string{"Golden Gate Bridge"},
			PostalCode:    []string{"94016"},
		},
		NotBefore:             time.Now(),
		NotAfter:              time.Now().AddDate(10, 0, 0),
		IsCA:                  true,
		ExtKeyUsage:           []x509.ExtKeyUsage{x509.ExtKeyUsageClientAuth, x509.ExtKeyUsageServerAuth},
		KeyUsage:              x509.KeyUsageDigitalSignature | x509.KeyUsageCertSign,
		BasicConstraintsValid: true,
	}

	// create our private and public key
	caPrivKey, err := rsa.GenerateKey(rand.Reader, 4096)
	if err != nil {
		return nil, nil, nil, err
	}

	// create the CA
	caBytes, err := x509.CreateCertificate(rand.Reader, ca, ca, &caPrivKey.PublicKey, caPrivKey)
	if err != nil {
		return nil, nil, nil, err
	}

	// pem encode
	caPEM := new(bytes.Buffer)
	err = pem.Encode(caPEM, &pem.Block{
		Type:  "CERTIFICATE",
		Bytes: caBytes,
	})
	if err != nil {
		return nil, nil, nil, err
	}

	caPrivKeyPEM := new(bytes.Buffer)
	err = pem.Encode(caPrivKeyPEM, &pem.Block{
		Type:  "RSA PRIVATE KEY",
		Bytes: x509.MarshalPKCS1PrivateKey(caPrivKey),
	})
	if err != nil {
		return nil, nil, nil, err
	}

	// set up our server certificate
	cert := &x509.Certificate{
		SerialNumber: big.NewInt(2019),
		Subject: pkix.Name{
			Organization:  []string{"Company, INC."},
			Country:       []string{"US"},
			Province:      []string{""},
			Locality:      []string{"San Francisco"},
			StreetAddress: []string{"Golden Gate Bridge"},
			PostalCode:    []string{"94016"},
		},
		IPAddresses:  []net.IP{net.IPv4(127, 0, 0, 1), net.IPv6loopback},
		NotBefore:    time.Now(),
		NotAfter:     time.Now().AddDate(10, 0, 0),
		SubjectKeyId: []byte{1, 2, 3, 4, 6},
		ExtKeyUsage:  []x509.ExtKeyUsage{x509.ExtKeyUsageClientAuth, x509.ExtKeyUsageServerAuth},
		KeyUsage:     x509.KeyUsageDigitalSignature,
	}

	certPrivKey, err := rsa.GenerateKey(rand.Reader, 4096)
	if err != nil {
		return nil, nil, nil, err
	}

	certBytes, err := x509.CreateCertificate(rand.Reader, cert, ca, &certPrivKey.PublicKey, caPrivKey)
	if err != nil {
		return nil, nil, nil, err
	}

	certPEM := new(bytes.Buffer)
	err = pem.Encode(certPEM, &pem.Block{
		Type:  "CERTIFICATE",
		Bytes: certBytes,
	})
	if err != nil {
		return nil, nil, nil, err
	}

	certPrivKeyPEM := new(bytes.Buffer)
	err = pem.Encode(certPrivKeyPEM, &pem.Block{
		Type:  "RSA PRIVATE KEY",
		Bytes: x509.MarshalPKCS1PrivateKey(certPrivKey),
	})
	if err != nil {
		return nil, nil, nil, err
	}

	/* serverCert, err := tls.X509KeyPair(certPEM.Bytes(), certPrivKeyPEM.Bytes())

	if err != nil {
		return nil, nil, err
	} */

	publicKey := certPEM.Bytes()
	privateKey := certPrivKeyPEM.Bytes()

	return &caBytes, &publicKey, &privateKey, nil
}

// WriteFile write a file to path with filedata - TODO: find a better spot for this....
func WriteFile(filepath string, filedata []byte) error {
	fp, err := os.OpenFile(filepath, os.O_WRONLY|os.O_CREATE, 0600) // #nosec G304
	if err != nil {
		return err
	}

	len, err := fp.Write(filedata)
	if err != nil {
		return err
	}

	fmt.Printf("%d bytes written into file.\n", len)
	err = fp.Close()
	if err != nil {
		return err
	}
	return nil
}
