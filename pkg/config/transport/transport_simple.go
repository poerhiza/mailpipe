package config

import (
	"encoding/json"
	"fmt"

	"gitlab.com/poerhiza/mailpipe/pkg/storage/crypto"
)

// TransportConfigSimple for basic sending of emails
type TransportConfigSimple struct {
	BaseTransportConfig
	Footer   string `json:"footer"`
	Host     string `json:"host"`
	Port     int    `json:"port"`
	Username string `json:"username,omitempty"`
	Password string `json:"password,omitempty"`
}

// GetAPIKey returns the APIKey for simple config
func (d *TransportConfigSimple) GetAPIKey(password string, salt string, status string) (string, error) {
	return "", fmt.Errorf("no such value")
}

// GetURL returns the APIKey for simple config
func (d *TransportConfigSimple) GetURL() (string, error) {
	return "", fmt.Errorf("no such value")
}

// GetFooter returns the APIKey for simple config
func (d *TransportConfigSimple) GetFooter() (string, error) {
	return d.Footer, nil
}

// GetHost returns the APIKey for simple config
func (d *TransportConfigSimple) GetHost() (string, error) {
	return d.Host, nil
}

// GetPort returns the APIKey for simple config
func (d *TransportConfigSimple) GetPort() (int, error) {
	return d.Port, nil
}

// GetUsername returns the APIKey for simple config
func (d *TransportConfigSimple) GetUsername(password string, salt string, status string) (string, error) {
	return crypto.AESGCMDecrypt(password, d.Username, []byte(salt), status == TransportConfigStatusSiteWide)
}

// GetPassword returns the APIKey for simple config
func (d *TransportConfigSimple) GetPassword(password string, salt string, status string) (string, error) {
	return crypto.AESGCMDecrypt(password, d.Password, []byte(salt), status == TransportConfigStatusSiteWide)
}

// MarshalString hack to return data as a json string
func (d *TransportConfigSimple) MarshalString() (string, error) {
	data, err := json.Marshal(d)

	if err != nil {
		return "", err
	}

	return string(data[:]), nil
}
