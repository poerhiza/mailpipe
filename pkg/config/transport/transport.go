package config

import (
	"encoding/json"
	"fmt"
	"net/url"
)

// TransportConfigStatusSiteWide transport config status type
const TransportConfigStatusSiteWide = "sitewide"

// TransportConfigStatusAccount transport config status type
const TransportConfigStatusAccount = "account"

// TransportConfigTypeSendgrid transport config type
const TransportConfigTypeSendgrid = "sendgrid"

// TransportConfigTypeSimple transport config type
const TransportConfigTypeSimple = "simple"

// TransportConfig used to define a transport for the API to leverage when sending emails
type TransportConfig struct {
	Data   TransportConfigData `json:"data"`
	Status string              `json:"status,omitempty"`
	Title  string              `json:"title"`
	ID     string              `json:"id,omitempty"`
}

// UnmarshalJSON hack to get JSON loaded in a dynamic fashion
func (b *TransportConfig) UnmarshalJSON(data []byte) error {
	var tmpHeader struct {
		Data struct {
			BaseTransportConfig
		} `json:"data"`
	}
	if err := json.Unmarshal(data, &tmpHeader); err != nil {
		return err
	}

	switch tmpHeader.Data.Type {
	case "sendgrid":
		b.Data = new(TransportConfigSendGrid)
	case "simple":
		b.Data = new(TransportConfigSimple)
	}

	type tmp TransportConfig // avoids infinite recursion
	return json.Unmarshal(data, (*tmp)(b))
}

// TransportConfigData lightweight plug
type TransportConfigData interface {
	GetType() string
	MarshalString() (string, error)
	GetAPIKey(string, string, string) (string, error)
	GetURL() (string, error)
	GetFooter() (string, error)
	GetHost() (string, error)
	GetPort() (int, error)
	GetUsername(string, string, string) (string, error)
	GetPassword(string, string, string) (string, error)
}

// BaseTransportConfig another lightweight plug
type BaseTransportConfig struct {
	Type string `json:"type"`
}

// GetType hack to return type and get dynamic json
func (d *BaseTransportConfig) GetType() string {
	return d.Type
}

// ParseURL returns the API URL for sendgrid config
func ParseURL(rawurl string) (string, string, error) {

	url, err := url.Parse(rawurl)

	if nil != err {
		return "", "", err
	}

	return fmt.Sprintf("%s://%s", url.Scheme, url.Host), url.RequestURI(), nil
}
