package config

import (
	"encoding/json"
	"fmt"

	"gitlab.com/poerhiza/mailpipe/pkg/storage/crypto"
)

// TransportConfigSendGrid for sendgrid configuration params
type TransportConfigSendGrid struct {
	BaseTransportConfig
	URL    string `json:"url"`
	APIKey string `json:"apikey"`
	Footer string `json:"footer,omitempty"`
}

// GetAPIKey returns the APIKey for sendgrid config
func (d *TransportConfigSendGrid) GetAPIKey(password string, salt string, status string) (string, error) {
	return crypto.AESGCMDecrypt(password, d.APIKey, []byte(salt), status == TransportConfigStatusSiteWide)
}

// GetURL returns the APIKey for sendgrid config
func (d *TransportConfigSendGrid) GetURL() (string, error) {
	return d.URL, nil
}

// GetFooter returns the APIKey for sendgrid config
func (d *TransportConfigSendGrid) GetFooter() (string, error) {
	return d.Footer, nil
}

// GetHost returns the APIKey for sendgrid config
func (d *TransportConfigSendGrid) GetHost() (string, error) {
	return "", fmt.Errorf("no such value gethost for sendgrid")
}

// GetPort returns the APIKey for sendgrid config
func (d *TransportConfigSendGrid) GetPort() (int, error) {
	return -1, fmt.Errorf("no such value getport for sendgrid")
}

// GetUsername returns the APIKey for sendgrid config
func (d *TransportConfigSendGrid) GetUsername(password string, salt string, status string) (string, error) {
	return "", fmt.Errorf("no such value getusername for sendgrid")
}

// GetPassword returns the APIKey for sendgrid config
func (d *TransportConfigSendGrid) GetPassword(password string, salt string, status string) (string, error) {
	return "", fmt.Errorf("no such value getpassword for sendgrid")
}

// MarshalString hack to return data as a json string
func (d *TransportConfigSendGrid) MarshalString() (string, error) {
	data, err := json.Marshal(d)

	if err != nil {
		return "", err
	}

	return string(data[:]), nil
}
