package config

// Common configuration struct which is used by both the API and Mailpipe proper
type Common struct {
	MTLS                     bool   `json:"mtls"`
	CertificateFile          string `json:"certificate"`
	KeyFile                  string `json:"key"`
	CertificateAuthorityFile string `json:"certificate_authority"`
	Host                     string `json:"host"`
	Port                     int    `json:"port"`
	AttachmentStoragePath    string `json:"attachment_storage_path"`
}
