package config

import (
	"crypto/tls"
	"crypto/x509"

	commonConfig "gitlab.com/poerhiza/mailpipe/pkg/config"
	transportConfig "gitlab.com/poerhiza/mailpipe/pkg/config/transport"
	"gitlab.com/poerhiza/mailpipe/pkg/storage/redis"
)

// MailpipeAPIConfig represents the json structure which is read in from the api.json file at runtime
type MailpipeAPIConfig struct {
	Common          commonConfig.Common               `json:"common"`
	Transports      []transportConfig.TransportConfig `json:"transports"`
	Redis           redis.Config                      `json:"redis"`
	Certificate     []tls.Certificate
	CertificatePool *x509.CertPool
	Debug           bool
	Verbose         bool
}
