package mailpipe

import (
	"fmt"
	"log"
	"net/mail"
	"regexp"
	"strings"
	"time"

	"github.com/jhillyerd/enmime"
	"golang.org/x/crypto/bcrypt"

	"gitlab.com/poerhiza/mailpipe/pkg/constants"
	"gitlab.com/poerhiza/mailpipe/pkg/logger"
	"gitlab.com/poerhiza/mailpipe/pkg/storage/crypto"
	"gitlab.com/poerhiza/mailpipe/pkg/storage/redis"
)

// EmailFilter is a struct for mailpipe and api to leverage for listing emails to be collected or not
type EmailFilter struct {
	emailConfig        map[string]*emailConfig
	Domains            map[string]bool
	Emails             map[string]bool
	MaxAttachmentCount int
}

type emailConfig struct {
	CatchAllBox string `json:"unread"`
	Password    string `json:"password"`
	Domain      string `json:"domain"`
	Handle      string `json:"handle"`
	Email       string `json:"email"`
	Rules       []mailpipeMappingRule
}

type mailpipeMappingRule struct {
	BoxKey   string
	Header   map[string]*regexp.Regexp
	Body     *regexp.Regexp
	Duration time.Duration
}

// NewEmailFilter return a new EmailFilter
func NewEmailFilter() *EmailFilter {
	return &EmailFilter{
		emailConfig:        make(map[string]*emailConfig),
		Emails:             make(map[string]bool),
		Domains:            make(map[string]bool),
		MaxAttachmentCount: 30,
	}
}

// Take in the mailpipeconfig for boxes and returns
func (ec *emailConfig) parseBoxesToRules(boxes []mailpipeMappingBox) {

	if len(boxes) <= 0 {
		return
	}

	ec.CatchAllBox = constants.GetBoxKey(ec.Email, constants.UnreadBoxName)

	for _, box := range boxes {
		boxkey := constants.GetBoxKey(ec.Email, box.Title)

		var mmr mailpipeMappingRule

		mmr.BoxKey = boxkey
		duration, err := time.ParseDuration(box.Expire)

		if err != nil {
			logger.Log("[!] Bad rule config for expire time " + box.Title)
			mmr.Duration = 0
		} else {
			mmr.Duration = duration
		}

		for _, rule := range box.Rules {

			ruleConfig := strings.Split(rule, ":")

			if ruleConfig[0] == "header" {

				if len(ruleConfig) < 2 {
					logger.Log("[!] Bad rule config for header " + rule)
					continue
				}

				var tmp []string
				regexString := strings.Join(append(tmp, ruleConfig[2:]...), "")
				regex, err := regexp.CompilePOSIX(regexString)

				if err != nil {
					logger.Log("[!] Failed to parse " + regexString)
					logger.Log(err.Error())
					continue
				}

				if mmr.Header == nil {
					mmr.Header = make(map[string]*regexp.Regexp)
				}

				// ruleConfig[1] is the header name to search (lowercase)
				mmr.Header[ruleConfig[1]] = regex
			}

			if ruleConfig[0] == "body" {
				logger.Log("[-] body is not implemented at this time")
			}
		}

		ec.Rules = append(ec.Rules, mmr)
	}
}

// GetAddressesFromEmail parses an enmime envelope by the headerlist for email addresses and returns them if parsed successfully by mail.ParseAddress
func (ef *EmailFilter) GetAddressesFromEmail(headerlist []string, env *enmime.Envelope) []*mail.Address {
	var contactlist []string
	var emails []*mail.Address

	for _, header := range headerlist {
		for _, to := range env.GetHeaderValues(header) {
			contactlist = append(contactlist, strings.Split(to, ",")...)
		}
	}

	for _, entry := range contactlist {
		email, err := mail.ParseAddress(strings.ToLower(entry))

		if err == nil {
			emails = append(emails, email)
		} else {
			logger.Log("[!] Failed to parse address: " + entry)
		}
	}

	return emails
}

// SaveContacts will update the contacts database for a user
func (ef *EmailFilter) SaveContacts(emailAddress string, env *enmime.Envelope) {
	headerlist := []string{"To", "From", "CC", "BCC"}
	addresses := ef.GetAddressesFromEmail(headerlist, env)

	for _, email := range addresses {

		var address string = email.Address
		var name string = email.Name

		if address == "" {
			continue
		}

		err := redis.AddMetadataToEntry(constants.GetContactsKey(emailAddress), address, name)
		if err != nil {
			logger.Log(err.Error())
			return
		}
	}
}

// MatchEmailToBoxes take an email address and check its rules for any matches with the defined headers
func (ef *EmailFilter) MatchEmailToBoxes(timestamp string, metadataKey string, emailAddress string, env *enmime.Envelope) {
	var configs []*emailConfig

	config, ok := ef.emailConfig[emailAddress]

	if !ok {
		logger.Log("[!] Unable to locate email config for " + emailAddress)

		currentPassword, err := redis.GetAccountPassword(emailAddress)

		if err != nil {
			log.Fatal(err)
		}

		if currentPassword == "" { // this is a new account

			ret := crypto.HashString(emailAddress)

			hashedPassword, err := bcrypt.GenerateFromPassword([]byte(ret[0:72]), bcrypt.DefaultCost)

			if err != nil {
				logger.Log(err.Error())
			}

			err = redis.SetAccountPassword(emailAddress, string(hashedPassword))

			if err != nil {
				log.Fatal(err)
			}
		}
	} else {
		configs = append(configs, config)
	}

	if !ok {
		domain := fmt.Sprintf("*@%s", strings.Split(emailAddress, "@")[1])
		config, ok = ef.emailConfig[domain]

		if !ok {
			logger.Log("[!] Unable to locate domain config for " + emailAddress)
		} else {
			configs = append(configs, config)
		}
	}

	if len(configs) > 0 {

		for _, config := range configs {

			for _, headerKey := range env.GetHeaderKeys() { //TODO: this is awful, can we not do better than N^2...?!...
				header := strings.ToLower(headerKey)

				for _, rule := range config.Rules {

					// TODO: rule.Body

					if rule.Header != nil {
						regex := rule.Header[header]

						if regex != nil {

							if regex.Match([]byte(env.GetHeader(header))) {

								if redis.AddBoxEntry(rule.BoxKey, timestamp, metadataKey) == nil {
									logger.Log("[I] Email saved to: " + rule.BoxKey + " for email address: " + emailAddress)
								}

								// the last rule to 'hit' for an email determines the real duration for which an email exists in the database
								if rule.Duration != 0 {
									err := redis.SetExpirationOnMetadataEntry(metadataKey, rule.Duration)

									if err != nil {
										log.Fatal(err)
									}
								}
							}
						}
					}
				}
			}
		}
	}

	// always save a light-weight 'copy' to the 'unread'
	err := redis.AddBoxEntry(constants.GetBoxKey(emailAddress, constants.UnreadBoxName), timestamp, metadataKey)

	if err != nil {
		logger.Log(err.Error())
	} else {
		logger.Log("[I] Email saved to global unread for " + emailAddress)
	}
}

// GetMatches returns any emails in a string "email@domain.tld,email@domain.tld" which match the configured filter
func (ef *EmailFilter) GetMatches(emails []*mail.Address, verbose bool) ([]string, error) {
	var hitAddr = make(map[string]string)

	for _, v := range emails {
		addr := v.Address

		if ef.Match(addr) {

			if verbose {
				logger.Log(fmt.Sprintf("[I] Hit! To: |%s|\n", addr))
			}

			hitAddr[addr] = addr

		} else if verbose {
			logger.Log(fmt.Sprintf("[I] not a Hit... To:|%s|\n", addr))
		}
	}

	keys := make([]string, 0, len(hitAddr))
	for k := range hitAddr {
		keys = append(keys, k)
	}

	return keys, nil
}

// Match supplied with an email address, return whether or not it matches the configured filters
func (ef *EmailFilter) Match(emailAddress string) bool {
	ret := false

	if _, ok := ef.emailConfig[emailAddress]; ok {
		ret = true
	}

	emailParts := strings.Split(emailAddress, "@")
	domain := ""

	if len(emailParts) == 2 {
		domain = emailParts[1]
	}

	if _, ok := ef.Domains[domain]; ok {
		ret = true
	}

	return ret
}
