package mailpipe

import (
	"crypto/tls"
	"crypto/x509"
	"fmt"
	"log"
	"net"
	"os"
	"strconv"
	"strings"
	"time"

	"github.com/chrj/smtpd"
	"github.com/jhillyerd/enmime"

	"gitlab.com/poerhiza/mailpipe/pkg/constants"
	"gitlab.com/poerhiza/mailpipe/pkg/logger"
	"gitlab.com/poerhiza/mailpipe/pkg/storage/crypto"
	"gitlab.com/poerhiza/mailpipe/pkg/storage/redis"
)

// Mailpipe struct to hold a running mailpipe
type Mailpipe struct {
	AttachmentStoragePath string
	Certificate           []tls.Certificate
	CAPool                *x509.CertPool
	Protocol              string
	Host                  string
	Port                  int
	Server                *smtpd.Server
	Listener              net.Listener
	Pid                   int
	Verbose               bool
	Debug                 bool
	EmailFilter           *EmailFilter
}

func (mp *Mailpipe) addAddrToBlocklist(source net.Addr) {
	blockListDuration, err := time.ParseDuration(fmt.Sprintf("%ds", constants.BlocklistTimer))

	if err != nil {
		logger.Log(err.Error())
		return
	}

	host, _, err := net.SplitHostPort(source.String())

	if err != nil {
		logger.Log(err.Error())
		return
	}

	err = redis.AddAddressToBlocklist(constants.GetBlockListKey(), host, fmt.Sprintf("%d", time.Now().Add(blockListDuration).Unix()))
	if err != nil {
		logger.Log(err.Error())
		return
	}

	err = redis.AddAddressToBlocklistCount(constants.GetBlockListCountKey(), host)
	if err != nil {
		logger.Log(err.Error())
		return
	}
}

func (mp *Mailpipe) parseAndStore(
	peer smtpd.Peer,
	envelope smtpd.Envelope,
) {

	if mp.Verbose {
		logger.Log("[I]" + string(envelope.Sender))
		logger.Log(fmt.Sprintf("[I] Sender: %s", peer.Addr.String()))
		logger.Log(fmt.Sprintf("[I] HeloName: %s", peer.HeloName))
		logger.Log(fmt.Sprintf("[I] Protocol: %s", peer.Protocol))
		logger.Log("[I]")
	}

	r := strings.NewReader(string(envelope.Data))

	env, err := enmime.ReadEnvelope(r)

	if err != nil {
		logger.Log(err.Error())
		return
	}

	err = env.AddHeader("X-MP-E-Peer", peer.Addr.String())
	if err != nil {
		logger.Log(err.Error())
	}

	err = env.AddHeader("X-MP-E-Helo", peer.HeloName)
	if err != nil {
		logger.Log(err.Error())
	}

	err = env.AddHeader("X-MP-E-ServerName", peer.ServerName)
	if err != nil {
		logger.Log(err.Error())
	}

	err = env.AddHeader("X-MP-E-Sender", envelope.Sender)
	if err != nil {
		logger.Log(err.Error())
	}

	contactlist := mp.EmailFilter.GetAddressesFromEmail([]string{"To", "CC", "BCC"}, env)
	hitAddr, err := mp.EmailFilter.GetMatches(contactlist, mp.Verbose)

	if err != nil {
		logger.Log(err.Error())
		return
	}

	if len(hitAddr) <= 0 {
		mp.collect(constants.SpamAddress, env)
		mp.EmailFilter.SaveContacts(constants.SpamAddress, env)
		go mp.addAddrToBlocklist(peer.Addr)
		return
	}

	for _, emailAddress := range hitAddr {
		mp.collect(emailAddress, env)
		mp.EmailFilter.SaveContacts(emailAddress, env)
	}
}

func requireSuccessToContinue(err error) {

	if err != nil {
		logger.Log(fmt.Sprintf("[!] %v", err))
		os.Exit(1)
	}
}

func (mp *Mailpipe) collect(
	emailAddress string,
	env *enmime.Envelope,
) {

	var timestamp time.Time = time.Now()

	if mp.Verbose {
		logger.Log(fmt.Sprintf("[I] Logging hit for:|%s| at %s\n", emailAddress, timestamp.Format(time.RFC3339)))
		logger.Log(fmt.Sprintf("[I]   Attachment count: %d", len(env.Attachments)))
	}

	metadataHashKey, metadataKey := constants.GetEmailKeyAndHash(env.GetHeader("From"), env.GetHeader("Subject"), timestamp)

	// TODO: Need this or no? requireSuccessToContinue(redis.CreateMetadataEntry(metadataKey))

	for _, header := range env.GetHeaderKeys() {
		requireSuccessToContinue(redis.AddMetadataToEntry(metadataHashKey, header, env.GetHeader(header)))
	}

	if len(env.Text) > 0 {
		requireSuccessToContinue(redis.AddMetadataToEntry(metadataHashKey, constants.MetadataKeyText, env.Text))
	}

	if len(env.HTML) > 0 {
		requireSuccessToContinue(redis.AddMetadataToEntry(metadataHashKey, constants.MetadataKeyHTML, env.HTML))
	}

	if (len(env.Attachments) > 0 && len(env.Attachments) < mp.EmailFilter.MaxAttachmentCount) || (len(env.Inlines) > 0 && len(env.Inlines) < mp.EmailFilter.MaxAttachmentCount) {
		mp.saveAttachments(env, metadataHashKey)
	}

	if len(env.Errors) > 0 {
		var errors = ""

		for _, envError := range env.Errors {
			errors = errors + "\n" + envError.Error()
		}

		requireSuccessToContinue(redis.AddMetadataToEntry(metadataHashKey, constants.MetadataKeyErrors, errors))
	}

	mp.EmailFilter.MatchEmailToBoxes(strconv.FormatInt(timestamp.Unix(), 10), metadataKey, emailAddress, env)
}

// SaveAttachments - save attachments to disk
func (mp *Mailpipe) saveAttachments(
	env *enmime.Envelope,
	metadataHashKey string,
) {

	savePath := constants.GetAttachmentsPath(mp.AttachmentStoragePath, metadataHashKey)

	if err := os.MkdirAll(savePath, 0750); err != nil {
		logger.Log(fmt.Sprintf("[!] Failed to create a directory for saving attachments... %s", savePath))
		return
	}

	var count = 0

	for _, attachment := range env.Attachments {
		count += savePart(attachment, metadataHashKey, savePath)
	}

	for _, attachment := range env.Inlines {
		count += savePart(attachment, metadataHashKey, savePath)
	}

	if count > 0 {
		requireSuccessToContinue(redis.AddMetadataToEntry(metadataHashKey, constants.MetadataKeyAttachmentCount, strconv.Itoa(count)))
	}
}

func savePart(
	attachment *enmime.Part,
	metadataHashKey string,
	savePath string,
) int {

	if attachment.FileName != "" {
		logger.Log("[D] Attachment: " + attachment.FileName)

		var attachmentHash = crypto.MD5HashString(attachment.FileName)
		var savedFilepath = constants.GetAttachmentsPath(savePath, attachmentHash)

		reader, err := os.Create(savedFilepath) /* #nosec G304 */
		if err != nil {
			logger.Log("[!] Failed to open file...")
			return 0
		}

		_, err = reader.Write(attachment.Content)
		if err != nil {
			log.Fatal(err)
		}

		err = reader.Close()
		if err != nil {
			log.Fatal(err)
		}

		err = redis.AddAttachmentEntry(metadataHashKey, attachmentHash, attachment.FileName, savedFilepath)
		if err != nil {
			log.Fatal(err)
		}
		return 1
	}
	return 0
}

// New mailpipe created - a new mail listener based on the provided options
func New(mailpipeConfig *Config) (
	Mailpipe,
	error,
) {

	if mailpipeConfig.Debug {
		logger.Log("[D] Entering mailpipe.New")
	}

	mp := Mailpipe{
		AttachmentStoragePath: mailpipeConfig.Common.AttachmentStoragePath,
		Certificate:           mailpipeConfig.Certificate,
		CAPool:                mailpipeConfig.CertificatePool,
		Protocol:              mailpipeConfig.Protocol,
		Host:                  mailpipeConfig.Common.Host,
		Port:                  mailpipeConfig.Common.Port,
		Server:                nil,
		Listener:              nil,
		Pid:                   os.Getpid(),
		Verbose:               mailpipeConfig.Verbose,
		Debug:                 mailpipeConfig.Debug,
		EmailFilter:           mailpipeConfig.EmailFilter,
	}

	if mp.Verbose {
		logger.Log("[I] Mailpipe Information")
		logger.Log(fmt.Sprintf("[I] Protocol: %s", mp.Protocol))
		logger.Log(fmt.Sprintf("[I] Bound to: %s:%d", mp.Host, mp.Port))
	}

	mp.Server = &smtpd.Server{
		Handler: func(peer smtpd.Peer, envelope smtpd.Envelope) error {
			go mp.parseAndStore(peer, envelope)
			return nil
		},
	}

	if mp.Debug {
		logger.Log("[D] Leaving mailpipe.New")
	}

	return mp, nil
}

// Run to attach the mailpipe and start listening
func (mp *Mailpipe) Run() error {

	if mp.Debug {
		logger.Log("[D] Entering mailpipe.Run")
	}

	if mp.Verbose {
		logger.Log("[I] Starting Listener")
	}

	addr := fmt.Sprintf("%s:%d", mp.Host, mp.Port)

	if mp.Protocol == "smtp" || mp.Protocol == "starttls" {
		lsnr, err := net.Listen("tcp", addr)

		if err != nil {
			return err
		}

		mp.Listener = lsnr
	}

	if mp.Protocol == "tls" || mp.Protocol == "starttls" {

		if mp.Certificate == nil {
			err := fmt.Errorf("[!] No certificate found")
			log.Fatal(err)
			return err
		}

		mp.Server.ForceTLS = true

		mp.Server.TLSConfig = &tls.Config{
			PreferServerCipherSuites: true,
			Renegotiation:            tls.RenegotiateOnceAsClient,
			MinVersion:               tls.VersionTLS12,
			MaxVersion:               tls.VersionTLS13,
			CipherSuites: []uint16{
				tls.TLS_AES_128_GCM_SHA256,
				tls.TLS_AES_256_GCM_SHA384,
				tls.TLS_CHACHA20_POLY1305_SHA256,
				tls.TLS_ECDHE_ECDSA_WITH_AES_128_GCM_SHA256,
				tls.TLS_ECDHE_ECDSA_WITH_AES_256_GCM_SHA384,
				tls.TLS_ECDHE_ECDSA_WITH_CHACHA20_POLY1305,
				tls.TLS_ECDHE_ECDSA_WITH_CHACHA20_POLY1305_SHA256,
				tls.TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256,
				tls.TLS_ECDHE_RSA_WITH_AES_256_GCM_SHA384,
				tls.TLS_ECDHE_RSA_WITH_CHACHA20_POLY1305,
				tls.TLS_ECDHE_RSA_WITH_CHACHA20_POLY1305_SHA256,
			},
			Certificates: mp.Certificate,
		}

		if mp.CAPool != nil {
			mp.Server.TLSConfig.ClientCAs = mp.CAPool
			mp.Server.TLSConfig.RootCAs = mp.CAPool
		}

		if mp.Protocol == "tls" {
			lsnr, err := tls.Listen("tcp", addr, mp.Server.TLSConfig)

			if err != nil {
				return err
			}

			mp.Listener = lsnr
		}
	}

	go mp.Server.Serve(mp.Listener)

	logger.Log(fmt.Sprintf("[I] listener started at %s", addr))

	if mp.Debug {
		logger.Log("[D] Leaving mailpipe.Run")
	}

	return nil
}

// Shutdown a mailpipe instance
func (mp *Mailpipe) Shutdown() error {

	if mp.Listener != nil {
		return mp.Listener.Close()
	}
	return nil
}
