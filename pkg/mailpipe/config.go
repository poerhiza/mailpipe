package mailpipe

import (
	"crypto/tls"
	"crypto/x509"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"strings"

	"github.com/fatih/color"
	"golang.org/x/crypto/bcrypt"

	"gitlab.com/poerhiza/mailpipe/pkg/config"
	"gitlab.com/poerhiza/mailpipe/pkg/constants"
	"gitlab.com/poerhiza/mailpipe/pkg/logger"
	"gitlab.com/poerhiza/mailpipe/pkg/storage/crypto"
	"gitlab.com/poerhiza/mailpipe/pkg/storage/redis"
)

type mailpipeFilter struct {
	Domain  string            `json:"domain"`
	Mapping []mailpipeMapping `json:"mapping"`
}

type mailpipeMapping struct {
	Handle   string               `json:"handle"`
	Boxes    []mailpipeMappingBox `json:"boxes"`
	Password string               `json:"password"`
}

type mailpipeMappingBox struct {
	Title  string   `json:"title"`
	Rules  []string `json:"rules"`
	Expire string   `json:"expire"`
}

// Config represents the json structure which is read in from the 'config'.json file at runtime
type Config struct {
	Common          config.Common    `json:"common"`
	Protocol        string           `json:"protocol"`
	AttachmentMax   int              `json:"attachment_max"`
	Filters         []mailpipeFilter `json:"filters"`
	Redis           redis.Config     `json:"redis"`
	EmailFilter     *EmailFilter
	Certificate     []tls.Certificate
	CertificatePool *x509.CertPool
	Debug           bool
	Verbose         bool
}

// LoadAndParseConfig loads and parses the config for an emailfilter
func LoadAndParseConfig(configFile string, verbose bool, debug bool) (*Config, error) {

	config, err := ioutil.ReadFile(configFile) // #nosec G304

	if err != nil {
		logger.Log(fmt.Sprintf("Unable to read config file: %s", err))
		return nil, err
	}

	var mailpipeConfig Config

	err = json.Unmarshal([]byte(config), &mailpipeConfig)

	if err != nil {
		logger.Log(fmt.Sprintf("Unable to unmarshal config: %s", err))
		return nil, err
	}

	mailpipeConfig.Protocol = strings.ToLower(mailpipeConfig.Protocol)

	switch strings.ToLower(mailpipeConfig.Protocol) {
	case "smtp":
	case "tls":
	case "starttls":

	default:
		return nil, fmt.Errorf("%s is not a valid protocol", mailpipeConfig.Protocol)
	}

	if mailpipeConfig.Common.Port == -1 {

		switch mailpipeConfig.Protocol {
		case "smtp":
			mailpipeConfig.Common.Port = 25
		case "tls":
			mailpipeConfig.Common.Port = 465
		case "starttls":
			mailpipeConfig.Common.Port = 587
		}
	}

	mailpipeConfig.EmailFilter = NewEmailFilter()

	if mailpipeConfig.AttachmentMax > 0 {
		mailpipeConfig.EmailFilter.MaxAttachmentCount = mailpipeConfig.AttachmentMax
	}

	mailpipeConfig.Verbose = verbose
	mailpipeConfig.Debug = debug

	if mailpipeConfig.Common.CertificateFile != "" && mailpipeConfig.Common.KeyFile != "" {
		cert, err := tls.LoadX509KeyPair(mailpipeConfig.Common.CertificateFile, mailpipeConfig.Common.KeyFile)

		if err != nil {
			log.Fatal(err.Error())
		}

		mailpipeConfig.Certificate = append(mailpipeConfig.Certificate, cert)
	} else if mailpipeConfig.Common.CertificateFile != "" || mailpipeConfig.Common.KeyFile != "" {
		log.Fatal("You must define both a cert and a key")
	}

	if mailpipeConfig.Common.MTLS && mailpipeConfig.Common.CertificateAuthorityFile == "" {
		log.Fatal("You must provide a ca if using mTLS")
	}

	mailpipeConfig.CertificatePool = nil

	if mailpipeConfig.Common.CertificateAuthorityFile != "" {
		caCert, err := ioutil.ReadFile(mailpipeConfig.Common.CertificateAuthorityFile)

		if err != nil {
			log.Fatal(err.Error())
		}

		mailpipeConfig.CertificatePool = x509.NewCertPool()

		mailpipeConfig.CertificatePool.AppendCertsFromPEM(caCert)
	}

	mailpipeConfig.Common.AttachmentStoragePath = fmt.Sprintf("%s%c%s%c%s", mailpipeConfig.Common.AttachmentStoragePath, os.PathSeparator, "data", os.PathSeparator, constants.AttachmentStorageDirectoryName)

	if err := os.MkdirAll(mailpipeConfig.Common.AttachmentStoragePath, 0750); err != nil {
		logger.Log(fmt.Sprintf("[!] Failed to create a directory for saving attachments... %s", mailpipeConfig.Common.AttachmentStoragePath))
		log.Fatal(err.Error())
	}

	_, err = redis.New(mailpipeConfig.Redis)

	if err != nil {
		logger.Log(err.Error())
		os.Exit(1)
	}

	for _, mailpipeConfigItem := range mailpipeConfig.Filters {
		var domain string = mailpipeConfigItem.Domain

		for _, mailpipeMappingItem := range mailpipeConfigItem.Mapping {
			ec := new(emailConfig)
			ec.Email = fmt.Sprintf("%s@%s", mailpipeMappingItem.Handle, domain)
			ec.Domain = domain
			ec.Handle = mailpipeMappingItem.Handle
			ec.Password = mailpipeMappingItem.Password

			ec.parseBoxesToRules(mailpipeMappingItem.Boxes)

			if ec.Password == "" {
				password := crypto.HashString(ec.Email)[0:72]

				hashedPassword, err := bcrypt.GenerateFromPassword([]byte(password), bcrypt.DefaultCost)

				if err != nil {
					logger.Log(err.Error())
					os.Exit(2)
				}

				ec.Password = string(hashedPassword)
			}

			mailpipeConfig.EmailFilter.Domains[ec.Domain] = true
			mailpipeConfig.EmailFilter.Emails[ec.Email] = true
			mailpipeConfig.EmailFilter.emailConfig[ec.Email] = ec

			requireSuccessToContinue(redis.SetAccountPassword(ec.Email, ec.Password))

			if verbose {
				logger.Log(fmt.Sprintf("[+] Added email: %s", ec.Email))
			}
		}
	}

	if verbose {
		color.Green("Mailpipes:\n")

		for email, eConf := range mailpipeConfig.EmailFilter.emailConfig {
			color.Green(fmt.Sprintf("\t %s => %s\n", email, eConf.Rules))
		}
	}

	logger.Log("[+] Mailpipe Configuration loaded")

	return &mailpipeConfig, nil
}
