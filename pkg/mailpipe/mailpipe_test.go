package mailpipe

import (
	"crypto/tls"
	"encoding/base64"
	"fmt"
	"log"
	"net"
	"net/smtp"
	"os"
	"strconv"
	"strings"
	"testing"
	"time"

	"github.com/mediocregopher/radix/v3"
	"golang.org/x/crypto/bcrypt"

	"gitlab.com/poerhiza/mailpipe/pkg/constants"
	"gitlab.com/poerhiza/mailpipe/pkg/logger"
	"gitlab.com/poerhiza/mailpipe/pkg/storage/crypto"
	"gitlab.com/poerhiza/mailpipe/pkg/storage/redis"
)

func TestMain(m *testing.M) {

	caBytes, publicKey, privateKey, err := crypto.GenerateCertificate()

	if err != nil {
		log.Fatal(err)
	}

	err = crypto.WriteFile("./ca.crt", *caBytes)

	if err != nil {
		log.Fatal(err)
	}

	err = crypto.WriteFile("./certificate.pem", *publicKey)

	if err != nil {
		log.Fatal(err)
	}

	err = crypto.WriteFile("./key.pem", *privateKey)

	if err != nil {
		log.Fatal(err)
	}

	redisConfig := redis.Config{
		Protocol: "tcp",
		Address:  "127.0.0.1:6379",
		Poolsize: 10,
	}

	_, err = redis.New(redisConfig)

	if err != nil {
		logger.Log(err.Error())
		os.Exit(1)
	}

	// TODO: nasty hack
	mpc, err := LoadAndParseConfig("./mailpipe_test.json", true, true)

	if err != nil {
		log.Fatal(err)
	}

	mpc.Protocol = "starttls"
	mpc.Common.Host = "127.0.0.1"
	mpc.Common.Port = 2525
	mpc.Common.AttachmentStoragePath = "./dist/temp/"

	mp, err := New(mpc)

	if err != nil {
		log.Fatal(err.Error())
	}

	err = mp.Run()

	if err != nil {
		log.Fatal(err.Error())
	}

	result := m.Run()

	err = mp.Shutdown()
	if err != nil {
		log.Fatal(err.Error())
	}

	err = os.Remove("./ca.crt")

	if err != nil {
		log.Fatal(err.Error())
	}

	err = os.Remove("./certificate.pem")

	if err != nil {
		log.Fatal(err.Error())
	}

	err = os.Remove("./key.pem")

	if err != nil {
		log.Fatal(err.Error())
	}

	os.Exit(result)
}

func sendMail(from string, to string, message string, useTLS bool, useStartTLS bool) error {
	smtpHost := "127.0.0.1"
	smtpPort := 2525

	conn, err := net.Dial("tcp", smtpHost+":"+strconv.Itoa(smtpPort))

	if err != nil {
		return err
	}

	// #nosec G402
	tlsconfig := &tls.Config{
		InsecureSkipVerify: true,
		ServerName:         smtpHost,
	}

	if useTLS {
		conn = tls.Client(conn, tlsconfig)
	}

	client, err := smtp.NewClient(conn, smtpHost)
	if err != nil {
		return err
	}

	hasStartTLS, _ := client.Extension("STARTTLS")
	if useStartTLS && hasStartTLS {
		fmt.Println("STARTTLS ...")
		if err = client.StartTLS(tlsconfig); err != nil {
			return err
		}
	}

	if err := client.Mail(from); err != nil {
		return err
	}

	if err := client.Rcpt(to); err != nil {
		return err
	}

	w, err := client.Data()
	if err != nil {
		return err
	}

	_, err = w.Write([]byte(message))
	if err != nil {
		return err
	}

	err = w.Close()
	if err != nil {
		return err
	}

	err = client.Quit()
	if err != nil {
		return err
	}

	time.Sleep(5 * time.Second)

	return nil
}

func TestMailpipeAcceptAnEmailViaSTARTTLSAndParseIt(t *testing.T) {
	from := "test@localhost.localhost"
	to := "someone@gmail.com"
	body := `<HTML><HEAD><TITLE></TITLE></HEAD><BODY bgcolor=#FFFFFF leftmargin=5 topmargin=5 rightmargin=5 bottommargin=5>
<FONT size=2 color=#000000 face="Arial">
<DIV>I am Diplomat&nbsp; Andrew Peter , the United Nations special diplomat assigned to deliver your Consignment (World Bank approved fund valued US$10,625,000.00. Ten Million Six Hundred and Twenty Five Thousand United&nbsp; States Dollars)</DIV>
<DIV>As a matter of urgency I hereby request you to immediately reconfirm your delivery details to enable me proceed without delays for the delivery of your consignment box to your door step. Please Kindly contact us here:&nbsp; officemail101@qq.com</DIV>
<DIV>Name of your nearest airport:</DIV>
<DIV>Your Age:</DIV>
<DIV>Yours Sincerely,</DIV>
<DIV>Diplomat Andrew Peter</DIV>
</FONT></BODY></HTML>`

	header := make(map[string]string)
	header["From"] = from
	header["To"] = to
	header["Subject"] = "Just the"
	header["MIME-Version"] = "1.0"
	header["Date"] = "Mon, 10 Feb 2020 15:26:55 -0800"
	header["Content-Type"] = `text/html;\r\ncharset="Windows-1251"`
	header["Content-Transfer-Encoding"] = "base64"

	message := ""
	for k, v := range header {
		message += fmt.Sprintf("%s: %s\r\n", k, v)
	}
	message += "\r\n" + base64.StdEncoding.EncodeToString([]byte(body))

	err := sendMail(from, to, message, false, true)

	if err != nil {
		t.Error(err.Error())
	}

	var keys []string
	currentTime := time.Now()
	timestamp := currentTime.Format("2006-01-02T15")

	err = redis.Do(radix.Cmd(&keys, "KEYS", fmt.Sprintf("someone@gmail.com|||*|||Just the|||%s*", timestamp)))

	if err != nil {
		t.Error(err.Error())
	}

	metadataEntryKey := keys[0]
	metadataHashKey := strings.Split(metadataEntryKey, "|||")[1]

	var metadata map[string]string

	err = redis.Do(radix.Cmd(&metadata, "HGETALL", metadataHashKey))

	if err != nil {
		t.Error(err.Error())
	}

	if _, found := metadata["text"]; !found {
		t.Error("Unable to isolate the 'text' key")
	}

	if !strings.Contains(metadata["text"], "Diplomat Andrew Peter") {
		t.Error("The 'text' saved was not correct")
	}

	if _, found := metadata["html"]; !found {
		t.Error("Unable to isolate the 'html' key")
	}

	if !strings.Contains(metadata["html"], "Diplomat Andrew Peter") {
		t.Error("The 'html' saved was not correct")
	}

	if _, found := metadata["errors"]; !found {
		t.Error("Unable to isolate the 'errors' key")
	}

	if !strings.Contains(metadata["errors"], "text/plain") {
		t.Error("The 'errors' saved was not correct")
	}

	if _, found := metadata["From"]; !found {
		t.Error("Unable to isolate the 'From' key")
	}

	if metadata["From"] != "test@localhost.localhost" {
		t.Error("The 'From' saved was not correct")
	}

	var accounts map[string]string

	err = redis.Do(radix.Cmd(&accounts, "HGETALL", constants.EmailDatabaseName))

	if err != nil {
		t.Error(err.Error())
	}

	var found bool = false

	for email, hashedPassword := range accounts {

		password := crypto.HashString(email)

		if email == "someone@gmail.com" && nil == bcrypt.CompareHashAndPassword([]byte(hashedPassword), []byte(password)) {
			found = true
		}
	}

	if !found {
		t.Error("The account was not auto-created with the default password for someone@gmail.com in the redis database: " + constants.EmailDatabaseName)
	}

	var items []string

	err = redis.Do(radix.Cmd(&items, "ZRANGE", "someone@gmail.com|||boxes|||unread", "0", "-1"))

	if err != nil {
		t.Error(err.Error())
	}

	if items[0] != metadataEntryKey {
		t.Error("Unread item was not equal to metadataEntryKey - somethings wrong...")
	}

	// clean-up
	err = redis.Do(radix.Cmd(nil, "DEL", metadataEntryKey, metadataHashKey, "someone@gmail.com|||boxes|||unread"))

	if err != nil {
		t.Error(err.Error())
	}

	err = redis.Do(radix.Cmd(nil, "HDEL", constants.EmailDatabaseName, "someone@gmail.com"))

	if err != nil {
		t.Error(err.Error())
	}
}

func TestMailpipeAcceptAnEmailWithAttachmentViaSTARTTLSAndParseIt(t *testing.T) {
	from := "test@localhost.localhost"
	to := "someone@gmail.com"
	body := `--f46d0435be6e0b909704fe1f374c
Content-Type: multipart/alternative; boundary=f46d0435be6e0b909204fe1f374a

--f46d0435be6e0b909204fe1f374a
Content-Type: text/plain; charset=UTF-8

I found the last pic. that I was looking for, it is attached. ; )

--f46d0435be6e0b909204fe1f374a
Content-Type: text/html; charset=UTF-8

<div dir="ltr">I found the last pic. that I was looking for, it is attached. ; )<br></div>

--f46d0435be6e0b909204fe1f374a--
--f46d0435be6e0b909704fe1f374c
Content-Type: text/plain; name="message.txt"
Content-Disposition: attachment; filename="message.txt"
Content-Transfer-Encoding: base64
X-Attachment-Id: f_hxkye6jw0

d2VsbCBkb25lIC0gYnV0IHRoZXJlIGlzIG5vdGhpbmcgb2YgaW50ZXJlc3QgaGVyZSBeX14K
--f46d0435be6e0b909704fe1f374c--
`

	header := make(map[string]string)
	header["From"] = from
	header["To"] = to
	header["Subject"] = "Just the attachment"
	header["MIME-Version"] = "1.0"
	header["Date"] = "Mon, 10 Feb 2020 15:26:55 -0800"
	header["Content-Type"] = `multipart/mixed; boundary=f46d0435be6e0b909704fe1f374c`

	message := ""
	for k, v := range header {
		message += fmt.Sprintf("%s: %s\r\n", k, v)
	}
	message += "\r\n" + body

	err := sendMail(from, to, message, false, true)

	if err != nil {
		t.Error(err.Error())
	}

	var keys []string
	currentTime := time.Now()
	timestamp := currentTime.Format("2006-01-02T15")

	err = redis.Do(radix.Cmd(&keys, "KEYS", fmt.Sprintf("someone@gmail.com|||*|||Just the attachment|||%s*", timestamp)))

	if err != nil {
		t.Error(err.Error())
	}

	metadataEntryKey := keys[0]
	metadataHashKey := strings.Split(metadataEntryKey, "|||")[1]

	var metadata map[string]string

	err = redis.Do(radix.Cmd(&metadata, "HGETALL", metadataHashKey))

	if err != nil {
		t.Error(err.Error())
	}

	if _, found := metadata["text"]; !found {
		t.Error("Unable to isolate the 'text' key")
	}

	if !strings.Contains(metadata["text"], "it is attached") {
		t.Error("The 'text' saved was not correct")
	}

	if _, found := metadata["html"]; !found {
		t.Error("Unable to isolate the 'html' key")
	}

	if !strings.Contains(metadata["html"], "the last pic.") {
		t.Error("The 'html' saved was not correct")
	}

	if _, found := metadata["From"]; !found {
		t.Error("Unable to isolate the 'From' key")
	}

	if metadata["From"] != "test@localhost.localhost" {
		t.Error("The 'From' saved was not correct")
	}

	if _, found := metadata["Attachments"]; !found {
		t.Error("Unable to isolate the 'Attachments' key")
	}

	if metadata["Attachments"] != "1" {
		t.Error("The 'Attachments' saved was not correct")
	}

	var attachmentKeys []string

	err = redis.Do(radix.Cmd(&attachmentKeys, "KEYS", fmt.Sprintf("%s|||attachments|||*", metadataHashKey)))

	if err != nil {
		t.Error(err.Error())
	}

	if len(attachmentKeys) <= 0 {
		t.Error("Unable to find the attachment key")
	}

	var attachmentKey = attachmentKeys[0]

	if !strings.Contains(attachmentKey, "message.txt") {
		t.Error("Attachment did not contain the correct filename")
	}

	var attachments []string

	err = redis.Do(radix.Cmd(&attachments, "SMEMBERS", attachmentKey))

	if err != nil {
		t.Error(err.Error())
	}

	if len(attachments) <= 0 {
		t.Error("Unable to find the attachment by the attachmentKey")
	}

	err = os.RemoveAll(fmt.Sprintf("./%s", metadataHashKey))

	if err != nil {
		t.Error(err.Error())
	}

	var accounts map[string]string

	err = redis.Do(radix.Cmd(&accounts, "HGETALL", constants.EmailDatabaseName))

	if err != nil {
		t.Error(err.Error())
	}

	var found bool = false

	for email, hashedPassword := range accounts {

		password := crypto.HashString(email)

		if email == "someone@gmail.com" && nil == bcrypt.CompareHashAndPassword([]byte(hashedPassword), []byte(password)) {
			found = true
		}
	}

	if !found {
		t.Error("The account was not auto-created with the default password for someone@gmail.com in the redis database: " + constants.EmailDatabaseName)
	}

	var items []string

	err = redis.Do(radix.Cmd(&items, "ZRANGE", "someone@gmail.com|||boxes|||unread", "0", "-1"))

	if err != nil {
		t.Error(err.Error())
	}

	if items[0] != metadataEntryKey {
		t.Error("Unread item was not equal to metadataEntryKey - somethings wrong...")
	}

	// clean-up
	err = redis.Do(radix.Cmd(nil, "DEL", metadataEntryKey, metadataHashKey, "someone@gmail.com|||boxes|||unread", attachmentKey))

	if err != nil {
		t.Error(err.Error())
	}

	err = redis.Do(radix.Cmd(nil, "HDEL", constants.EmailDatabaseName, "someone@gmail.com"))

	if err != nil {
		t.Error(err.Error())
	}
}
