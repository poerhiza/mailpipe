package constants

import (
	"fmt"
	"os"
	"time"

	"gitlab.com/poerhiza/mailpipe/pkg/storage/crypto"
)

// API Request example
type APIRequest struct {
	AccountHash string `json:"account_hash" example:"5f4dcc3b5aa765d61d8327deb882cf99"`
}

// Seperator which is used to seperate keys
const Seperator = "|||"

// BlocklistTimer is the number of seconds in the future for which this blocklist keeps entries
const BlocklistTimer = 60

// SpamAddress is where all emails which are not hits (sent to this domain) are logged
const SpamAddress = "spam@localhost"

// MetadataKeyText is the key used for the text portion of an email if it exists within the email
const MetadataKeyText = "text"

// MetadataKeyHTML is the key used for the html portion of an email if it exists within the email
const MetadataKeyHTML = "html"

// MetadataKeyErrors is the key used if an error occured when parsing the email
const MetadataKeyErrors = "errors"

// MetadataKeyContacts is the key used to store an accounts contact list
const MetadataKeyContacts = "contacts"

// MetadataKeyTransport is the key used to store an accounts contact list
const MetadataKeyTransport = "transports"

// MetadataKeyAttachmentCount is a special key used track the number of attachments this email had
const MetadataKeyAttachmentCount = "Attachments"

// EmailDatabaseName the database/table which holds all the 'registered' emails
const EmailDatabaseName = "emails"

// BoxesDatabaseName the database/table name to append to an email address for listing their inboxes
const BoxesDatabaseName = "boxes"

// UnreadBoxName the box in to which anything that does not match a rule goes
const UnreadBoxName = "unread"

// ReadBoxName the box in to which anything that does not match a rule goes
const ReadBoxName = "read"

// AttachmentStorageDirectoryName the directory under which to save attachments - also a redis key part
const AttachmentStorageDirectoryName = "attachments"

// GetBlockListKey returns the blocklistKey for redis to store those bad guys in
func GetBlockListKey() string {
	return "sender_blocklist"
}

// GetBlockListCountKey returns the blocklistCountKey which is used to keep track of 'bad hosts' by address
func GetBlockListCountKey() string {
	return "sender_host_blocklist"
}

// GetAttachmentsPath returns the save path for attachments formated as it should be
func GetAttachmentsPath(storagePath string, metadataHashKey string) string {
	return fmt.Sprintf("%s%c%s", storagePath, os.PathSeparator, metadataHashKey)
}

// GetAttachmentPath returns the path for a single attachment
func GetAttachmentPath(storagePath string, metadataHashKey string, attachmenthash string) string {
	return fmt.Sprintf("%s%c%s", GetAttachmentsPath(storagePath, metadataHashKey), os.PathSeparator, attachmenthash)
}

// GetAttachmentKey returns the key used for the set that will contain an entry for where the attachment is stored
func GetAttachmentKey(metadataHashKey string, attachmentHash string, filename string) string {
	return fmt.Sprintf("%s%s%s%s%s%s%s", metadataHashKey, Seperator, AttachmentStorageDirectoryName, Seperator, attachmentHash, Seperator, filename)
}

// GetTransportTitle  returns the title by type for a transport
func GetTransportTitle(title string, titleType string) string {
	return fmt.Sprintf("%s%s%s", crypto.MD5HashString(fmt.Sprintf("%s%s", titleType, title)), Seperator, title)
}

// GetTransportKey returns the transport key used in the database for storing SMTP configuration's
func GetTransportKey(transportType string) string {
	return fmt.Sprintf("%s%s%s", MetadataKeyTransport, Seperator, transportType)
}

// GetContactsKey returns the contacts key used in the database for storing email contacts (hash)
func GetContactsKey(emailAddress string) string {
	return fmt.Sprintf("%s%s%s", emailAddress, Seperator, MetadataKeyContacts)
}

// AccountLinkRequest returns a tablename for a handle
func AccountLinkRequest(email string) string {
	return fmt.Sprintf("%s%saccount_link_requests", email, Seperator)
}

// AccountLink returns a tablename for account links
func AccountLink(email string) string {
	return fmt.Sprintf("%s%saccount_links", email, Seperator)
}

// AccountLinkRequestAccepted constant
const AccountLinkRequestAccepted = "accepted"

// AccountLinkRequestDenied constant
const AccountLinkRequestDenied = "denied"

// AccountLinkRequestPending constant
const AccountLinkRequestPending = "pending"

// GetBoxKeyBase returns the boxkey minus the title
func GetBoxKeyBase(email string) string {
	return fmt.Sprintf("%s%s%s%s", email, Seperator, BoxesDatabaseName, Seperator)
}

// GetBoxKey returns the boxkey for an email and box name
func GetBoxKey(email string, title string) string {
	return fmt.Sprintf("%s%s", GetBoxKeyBase(email), title)
}

// GetEmailKey returns the redis key for an email
func GetEmailKey(emailAddress string, emailMetadataHashKey string, emailSubject string, datetime string) string {
	return fmt.Sprintf("%s%s%s%s%s%s%s", emailAddress, Seperator, emailMetadataHashKey, Seperator, emailSubject, Seperator, datetime)
}

// GetEmailKeyAndHash returns the hash of the email key which is composed of varius pieces of the email datums
func GetEmailKeyAndHash(
	emailAddress string,
	emailSubject string,
	timestamp time.Time,
) (string, string) {

	emailKeyHash := crypto.MD5HashString(fmt.Sprintf("%s%s%s%s%s", emailAddress, Seperator, emailSubject, Seperator, timestamp.Format(time.RFC3339)))
	emailKey := GetEmailKey(emailAddress, emailKeyHash, emailSubject, timestamp.Format(time.RFC3339))

	return emailKeyHash, emailKey
}
