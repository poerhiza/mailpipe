package api

import (
	"crypto/tls"
	"crypto/x509"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"os"

	"github.com/fatih/color"

	apiConfig "gitlab.com/poerhiza/mailpipe/pkg/config/api"
	"gitlab.com/poerhiza/mailpipe/pkg/constants"
	"gitlab.com/poerhiza/mailpipe/pkg/logger"
)

// LoadAndParseConfig loads and parses the config for an emailfilter
func LoadAndParseConfig(configFile string, verbose bool, debug bool) (*apiConfig.MailpipeAPIConfig, error) {

	if configFile == "" {
		log.Fatal("You shouldn't be calling load and parse until things are more setup - read the docs my dude...")
	}

	config, err := ioutil.ReadFile(configFile) // #nosec G304

	if err != nil {
		logger.Log(fmt.Sprintf("Unable to read config file: %s", err))
		return nil, err
	}

	var apiConfig apiConfig.MailpipeAPIConfig

	err = json.Unmarshal([]byte(config), &apiConfig)

	if err != nil {
		logger.Log(fmt.Sprintf("Unable to unmarshal config: %s", err))
		return nil, err
	}

	apiConfig.Verbose = verbose
	apiConfig.Debug = debug

	if apiConfig.Verbose {
		color.Green("API Transports:\n")

		for _, item := range apiConfig.Transports {
			color.Green(fmt.Sprintf("\t %s\n", item.Data.GetType()))
		}
	}

	if apiConfig.Common.CertificateFile != "" && apiConfig.Common.KeyFile != "" {
		cert, err := tls.LoadX509KeyPair(apiConfig.Common.CertificateFile, apiConfig.Common.KeyFile)

		if err != nil {
			log.Fatal(err.Error())
		}

		apiConfig.Certificate = append(apiConfig.Certificate, cert)
	} else if apiConfig.Common.CertificateFile != "" || apiConfig.Common.KeyFile != "" {
		log.Fatal("You must define both a cert and a key")
	}

	if apiConfig.Common.MTLS && apiConfig.Common.CertificateAuthorityFile == "" {
		log.Fatal("You must provide a ca if using mTLS")
	}

	apiConfig.CertificatePool = nil

	if apiConfig.Common.CertificateAuthorityFile != "" {
		caCert, err := ioutil.ReadFile(apiConfig.Common.CertificateAuthorityFile) // #nosec G304

		if err != nil {
			log.Fatal(err.Error())
		}

		apiConfig.CertificatePool = x509.NewCertPool()

		apiConfig.CertificatePool.AppendCertsFromPEM(caCert)
	}

	apiConfig.Common.AttachmentStoragePath = fmt.Sprintf("%s%c%s%c%s", apiConfig.Common.AttachmentStoragePath, os.PathSeparator, "data", os.PathSeparator, constants.AttachmentStorageDirectoryName)

	if err := os.MkdirAll(apiConfig.Common.AttachmentStoragePath, 0750); err != nil {
		logger.Log(fmt.Sprintf("[!] Failed to create a directory for saving attachments... %s", apiConfig.Common.AttachmentStoragePath))
		log.Fatal(err.Error())
	}

	logger.Log("[+] Mailpipe API Configuration loaded")

	return &apiConfig, nil
}
