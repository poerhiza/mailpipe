package controller

import (
	"net/http"
	"path/filepath"
	"strings"

	"github.com/gin-gonic/gin"

	mailpipeGUI "gitlab.com/poerhiza/mailpipe-gui-svelte/dist"
)

var dist = mailpipeGUI.Dist()

// Static returns the static GUI and passes all API calls along
func (c *Controller) Static(ctx *gin.Context) {

	if strings.HasPrefix(ctx.Request.RequestURI, "/api") {
		ctx.Next()
	} else if strings.HasPrefix(ctx.Request.RequestURI, "/swagger") {
		ctx.Next()
	} else {
		path := strings.Replace(ctx.Request.URL.Path, "/", "", 1)

		var fileContent []byte

		if path == "" {
			path = "index.html"
		}

		var hasTried = false

	retry:

		fileContent, _ = dist.ReadFile(path)

		if !hasTried && fileContent == nil {
			path = "404.jpg"
			hasTried = true
			goto retry
		}

		var contentType string

		switch filepath.Ext(path) {
		case ".css":
			contentType = "text/css"
		case ".eot":
			contentType = "application/vnd.ms-fontobject"
		case ".html":
			contentType = "text/html"
		case ".js":
			contentType = "text/javascript"
		case ".jpg":
			contentType = "image/jpg"
		case ".svg":
			contentType = "image/svg+xml"
		case ".ttf":
			contentType = "font/ttf"
		case ".woff":
			contentType = "font/woff"
		case ".woff2":
			contentType = "font/woff2"
		default:
			contentType = "text/plain"
		}

		ctx.Data(http.StatusOK, contentType, fileContent)
	}
}
