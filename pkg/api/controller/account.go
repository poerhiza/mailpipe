package controller

import (
	"fmt"
	"log"
	"net/http"
	"strings"

	sessions "github.com/Calidity/gin-sessions"
	"github.com/gin-gonic/gin"
	"github.com/gin-gonic/gin/binding"

	"gitlab.com/poerhiza/mailpipe/pkg/api/httputil"
	"gitlab.com/poerhiza/mailpipe/pkg/api/model"
	"gitlab.com/poerhiza/mailpipe/pkg/constants"
	"gitlab.com/poerhiza/mailpipe/pkg/logger"
	"gitlab.com/poerhiza/mailpipe/pkg/storage/crypto"
	"gitlab.com/poerhiza/mailpipe/pkg/storage/redis"
)

// Authenticate godoc
// @Summary Authenticate as a user for an account
// @Description login a user to an account so they can view the unread
// @Tags account
// @Accept  json
// @Produce  json
// @Success 200
// @Param account body model.Account true "Authenticate an account"
// @Param action path string true "login or logout" Format(string)
// @Failure 400 {object} httputil.HTTPError
// @Failure 404 {object} httputil.HTTPError
// @Failure 500 {object} httputil.HTTPError
// @Router /account/state/authentication/{action} [post]
func (c *Controller) Authenticate(ctx *gin.Context) {

	action := ctx.Param("action")
	session := sessions.Default(ctx)
	email := session.Get("email")

	var account model.Account
	var unread int = 0

	if err := ctx.ShouldBindJSON(&account); err != nil {
		httputil.NewError(ctx, http.StatusBadRequest, err)
		return
	}

	if action == "change" {

		if email == nil || email.(string) == "" {
			httputil.NewError(ctx, http.StatusNotFound, fmt.Errorf("account not found"))
			return
		}

		account.Email = email.(string)

		err := model.ChangePassword(account)

		if err != nil {
			httputil.NewError(ctx, http.StatusBadRequest, err)
			return
		}
	}

	if action == "login" {

		if !model.Authenticate(account) {
			httputil.NewError(ctx, http.StatusForbidden, fmt.Errorf("unable to authenticate"))
			return
		}

		session.Set("emailhash", crypto.HashString(account.Email+account.Password))
		session.Set("email", account.Email)
		session.Set("user_tk", account.TK)
	}

	if action == "logout" || action == "change" {
		session.Clear()
	}

	err := session.Save()
	if err != nil {
		logger.Log(err.Error())
		logger.Log("Failed to save session...")
	}

	ctx.JSON(http.StatusOK, gin.H{"success": true, "unread": unread})
}

// GetLinkedAccounts godoc
// @Summary Returns the linked accounts or link account requests for the currently authenticated account
// @Description login a user to an account and link the account to view their boxes
// @Tags account
// @Accept  json
// @Produce  json
// @Success 200
// @Failure 400 {object} httputil.HTTPError
// @Failure 404 {object} httputil.HTTPError
// @Failure 500 {object} httputil.HTTPError
// @Router /account/state/link [get]
func (c *Controller) GetLinkedAccounts(ctx *gin.Context) {
	session := sessions.Default(ctx)

	accounts, err := redis.GetLinkedAccounts(session.Get("email").(string))

	if err != nil {
		logger.Log(err.Error())
		httputil.NewError(ctx, http.StatusNotFound, fmt.Errorf("message not found"))
		return
	}

	var data = make(map[string]model.AccountLink, len(accounts)-1)

	for email, hash := range accounts {

		unreadCount, err := redis.GetAccountUnreadCount(email)

		if err != nil {
			httputil.NewError(ctx, http.StatusInternalServerError, err)
			return
		}

		data[email] = model.AccountLink{
			Hash:   hash,
			Unread: unreadCount,
		}
	}

	ctx.JSON(http.StatusOK, gin.H{
		"accounts": data,
	})
}

// ListLinkedBox godoc
// @Summary Returns the linked accounts or link account requests for the currently authenticated account
// @Description login a user to an account and link the account to view their boxes
// @Tags account
// @Accept  json
// @Produce  json
// @Success 200
// @Failure 400 {object} httputil.HTTPError
// @Failure 404 {object} httputil.HTTPError
// @Failure 500 {object} httputil.HTTPError
// @Router /account/state/link/{box} [post]
func (c *Controller) ListLinkedBox(ctx *gin.Context) {
	var data []model.LinkedEmailBox

	box := ctx.Param("box")
	session := sessions.Default(ctx)
	email := session.Get("email")

	accounts, err := redis.GetLinkedAccounts(email.(string))

	if err != nil {
		logger.Log(err.Error())
		httputil.NewError(ctx, http.StatusNotFound, fmt.Errorf("message not found"))
		return
	}

	// include our active account in the list of accounts to fetch box messages for
	accounts[ctx.GetString("email")] = ctx.GetString("emailhash")

	for email, email_hash := range accounts {

		var boxMessages model.LinkedEmailBox

		boxMapping, err := redis.GetBoxListing(email, box)

		if err != nil {
			log.Fatal(err)
			return
		}

		for i := len(boxMapping) - 1; i >= 0; i-- { //reverse sort since inserts on sorted sets are FILO
			item := (boxMapping[i])

			boxEntry := strings.Split(item, constants.Seperator)

			emailFrom, err := redis.GetBoxEntryKey(boxEntry[1], "From")

			if err != nil {
				logger.Log(err.Error())
				emailFrom = "Unknown Sender"
			}

			boxMessages.Messages = append(boxMessages.Messages, model.BoxItem{
				Email:    emailFrom,
				ID:       boxEntry[1],
				Subject:  boxEntry[2],
				Datetime: boxEntry[3],
			})
		}

		if len(boxMessages.Messages) <= 0 {
			boxMessages.Messages = make([]model.BoxItem, 0)
		}

		boxMessages.EmailHash = email_hash
		boxMessages.Email = email

		data = append(data, boxMessages)
	}

	if len(data) <= 0 {
		data = make([]model.LinkedEmailBox, 0)
	}

	ctx.JSON(http.StatusOK, data)
}

// RequestLink godoc
// @Summary Provides a link request to an account
// @Description when a user logs in they should approve or disprove an account link request
// @Tags account
// @Accept  json
// @Produce  json
// @Success 200
// @Failure 400 {object} httputil.HTTPError
// @Failure 404 {object} httputil.HTTPError
// @Failure 500 {object} httputil.HTTPError
// @Router /account/state/link/request [post]
func (c *Controller) RequestLink(ctx *gin.Context) {
	session := sessions.Default(ctx)

	var accountLinkTransaction model.AccountLinkRequest

	if err := ctx.ShouldBindBodyWith(&accountLinkTransaction, binding.JSON); err != nil {
		httputil.NewError(ctx, http.StatusBadRequest, err)
		return
	}

	if accountLinkTransaction.Email == "" {
		httputil.NewError(ctx, http.StatusBadRequest, fmt.Errorf("unable to retrieve email from session"))
		return
	}

	ok, err := redis.SetLinkRequest(session.Get("email").(string), accountLinkTransaction.Email, constants.AccountLinkRequestPending)

	if ok {
		ctx.JSON(http.StatusOK, gin.H{"success": true})
	} else {
		httputil.NewError(ctx, http.StatusInternalServerError, err)
	}
}

// ReplyRequestLink godoc
// @Summary Approves or denies a request to link accounts
// @Description When a user requests to link an account the other account must approve or deny the request before the account will be linked
// @Tags account
// @Accept  json
// @Produce  json
// @Success 200
// @Failure 400 {object} httputil.HTTPError
// @Failure 404 {object} httputil.HTTPError
// @Failure 500 {object} httputil.HTTPError
// @Router /account/state/link/reply/:action [post]
func (c *Controller) ReplyRequestLink(ctx *gin.Context) {
	session := sessions.Default(ctx)
	email := session.Get("email").(string)
	account_hash := session.Get("emailhash").(string)

	var accountLinkTransaction model.AccountLinkRequest

	if err := ctx.ShouldBindBodyWith(&accountLinkTransaction, binding.JSON); err != nil {
		httputil.NewError(ctx, http.StatusBadRequest, err)
		return
	}

	action := ctx.Param("action")

	if action != constants.AccountLinkRequestAccepted && action != constants.AccountLinkRequestDenied {
		httputil.NewError(ctx, http.StatusBadRequest, fmt.Errorf("bad action"))
		return
	}

	if accountLinkTransaction.Email == "" {
		httputil.NewError(ctx, http.StatusBadRequest, fmt.Errorf("unable to retrieve email from session"))
		return
	}

	var ok bool
	var err error

	if action == constants.AccountLinkRequestAccepted {
		ok, err = redis.LinkRequestAccept(accountLinkTransaction.Email, email, account_hash)
	}

	if action == constants.AccountLinkRequestDenied {
		ok, err = redis.LinkRequestDeny(accountLinkTransaction.Email, email)
	}

	if ok {
		ctx.JSON(http.StatusOK, gin.H{"success": true})
	} else {
		httputil.NewError(ctx, http.StatusInternalServerError, err)
	}
}

// ListLinkRequests godoc
// @Summary Returns all of the pending account link requests for this account
// @Description When a user requests to link an account the other account must approve or deny the request before the account will be linked, this method returns all the pending requests
// @Tags account
// @Accept  json
// @Produce  json
// @Success 200
// @Failure 400 {object} httputil.HTTPError
// @Failure 404 {object} httputil.HTTPError
// @Failure 500 {object} httputil.HTTPError
// @Router /account/state/link/request [get]
func (c *Controller) ListLinkRequests(ctx *gin.Context) {
	session := sessions.Default(ctx)

	requests, err := redis.GetLinkRequests(session.Get("email").(string))

	if err != nil {
		logger.Log(err.Error())
		httputil.NewError(ctx, http.StatusNotFound, fmt.Errorf("message not found"))
		return
	}

	ctx.JSON(http.StatusOK, gin.H{
		"requests": requests,
	})
}

// DeleteLink godoc
// @Summary Removes an account link
// @Description This method will remove a link between accounts
// @Tags account
// @Accept  json
// @Produce  json
// @Success 200
// @Failure 400 {object} httputil.HTTPError
// @Failure 404 {object} httputil.HTTPError
// @Failure 500 {object} httputil.HTTPError
// @Router /account/state/link/remove/{perspective} [post]
func (c *Controller) DeleteLink(ctx *gin.Context) {
	perspective := ctx.Param("perspective")

	session := sessions.Default(ctx)
	email := session.Get("email").(string)

	if perspective == "" {
		httputil.NewError(ctx, http.StatusInternalServerError, fmt.Errorf("no perspective do I have"))
		return
	}

	var accountLinkTransaction model.AccountLinkRequest

	if err := ctx.ShouldBindBodyWith(&accountLinkTransaction, binding.JSON); err != nil {
		httputil.NewError(ctx, http.StatusBadRequest, err)
		return
	}

	var approverEmail string
	var requestorEmail string

	if perspective == "link" {
		requestorEmail = accountLinkTransaction.Email
		approverEmail = email
	}

	if perspective == "access" {
		requestorEmail = email
		approverEmail = accountLinkTransaction.Email
	}

	err := redis.RemoveAccountLink(requestorEmail, approverEmail)

	if err != nil {
		logger.Log(err.Error())
		httputil.NewError(ctx, http.StatusNotFound, fmt.Errorf("unable to remove link"))
		return
	}

	ctx.JSON(http.StatusOK, gin.H{
		"success": true,
	})
}
