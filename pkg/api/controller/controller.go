package controller

import (
	"fmt"
	"net"
	"net/http"
	"os"
	"regexp"
	"strings"

	sessions "github.com/Calidity/gin-sessions"
	"github.com/gin-gonic/gin"
	"github.com/gin-gonic/gin/binding"

	"gitlab.com/poerhiza/mailpipe/pkg/api/httputil"
	"gitlab.com/poerhiza/mailpipe/pkg/constants"
	"gitlab.com/poerhiza/mailpipe/pkg/logger"
	"gitlab.com/poerhiza/mailpipe/pkg/storage/redis"
)

// Controller example
type Controller struct {
	RegexEmailHash     *regexp.Regexp
	RegexBox           *regexp.Regexp
	RegexContactsQuery *regexp.Regexp
	Password           string
	Salt               string
	AllowOrigin        string
	Debug              bool
}

// Initialize setup controller with defaults
func Initialize(c *Controller) error {
	c.Password = os.Getenv("MAILPIPE_API_CONFIG_PASSWORD")
	c.Salt = os.Getenv("MAILPIPE_API_CONFIG_SALT")
	c.AllowOrigin = os.Getenv("MAILPIPE_API_CONFIG_ALLOW_ORIGIN")

	err := os.Setenv("MAILPIPE_API_CONFIG_PASSWORD", "")
	if err != nil {
		logger.Log("[!] Failed to setenv MAILPIPE_API_CONFIG_PASSWORD")
		logger.Log(err.Error())
		return err
	}

	err = os.Setenv("MAILPIPE_API_CONFIG_SALT", "")
	if err != nil {
		logger.Log("[!] Failed to setenv MAILPIPE_API_CONFIG_SALT")
		logger.Log(err.Error())
		return err
	}

	if c.Password == "" {
		return fmt.Errorf("[!] you must define MAILPIPE_API_CONFIG_PASSWORD")
	}

	if c.Salt == "" {
		return fmt.Errorf("[!] you must define MAILPIPE_API_CONFIG_SALT")
	}

	regex, err := regexp.CompilePOSIX("^[a-zA-Z0-9]+$")

	if err != nil {
		logger.Log("[!] Failed to parse regex for email hash validation")
		logger.Log(err.Error())
		return err
	}

	c.RegexEmailHash = regex

	c.RegexBox, err = regexp.CompilePOSIX("^[a-zA-Z0-9_]+$")

	if err != nil {
		logger.Log("[!] Failed to parse regex for email hash validation")
		logger.Log(err.Error())
		return err
	}

	c.RegexContactsQuery, err = regexp.CompilePOSIX("^[-a-zA-Z0-9_ @.]+$")

	if err != nil {
		logger.Log("[!] Failed to parse regex for email hash validation")
		logger.Log(err.Error())
		return err
	}

	return nil
}

// CORS for the world
func (c *Controller) CORS(ctx *gin.Context) {

	ctx.Writer.Header().Set("Access-Control-Allow-Origin", c.AllowOrigin)
	ctx.Writer.Header().Set("Access-Control-Allow-Credentials", "true")
	ctx.Writer.Header().Set("Access-Control-Allow-Headers", "Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization, accept, origin, Cache-Control, X-Requested-With")
	ctx.Writer.Header().Set("Access-Control-Allow-Methods", "POST, OPTIONS, GET, PUT, DELETE")

	if ctx.Request.Method == "OPTIONS" {
		ctx.AbortWithStatus(204)
		return
	}

	ctx.Next()
}

// EmailHashValidation will validate an email hash is in the correct format
func (c *Controller) EmailHashValidation(ctx *gin.Context) {

	session := sessions.Default(ctx)

	eh := session.Get("emailhash")
	e := session.Get("email")

	if eh == nil || e == nil {
		httputil.NewError(ctx, http.StatusBadRequest, fmt.Errorf("email or emailhash invalid"))
		return
	}

	// for linked account resources only
	if strings.Contains(ctx.Request.RequestURI, "/view") ||
		strings.Contains(ctx.Request.RequestURI, "/send") ||
		strings.Contains(ctx.Request.RequestURI, "/contacts") {

		var apirequest constants.APIRequest

		if ctx.Request.ContentLength <= 0 {
			httputil.NewError(ctx, http.StatusBadRequest, fmt.Errorf("account_hash missing"))
			return
		}

		if err := ctx.ShouldBindBodyWith(&apirequest, binding.JSON); err != nil {
			httputil.NewError(ctx, http.StatusBadRequest, err)
			return
		}

		accounts, err := redis.GetLinkedAccounts(e.(string))

		if err != nil {
			httputil.NewError(ctx, http.StatusInternalServerError, err)
			return
		}

		found := false

		for account, hash := range accounts {

			if hash == apirequest.AccountHash {
				found = true
				e = account
				break
			}
		}

		if !found && apirequest.AccountHash != eh.(string) {
			httputil.NewError(ctx, http.StatusForbidden, fmt.Errorf("account not authorized - send a link request first"))
			return
		}
	}

	ctx.Set("email", e.(string))
	ctx.Set("emailhash", eh.(string))

	ctx.Next()
}

var emailRegex = regexp.MustCompile("^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$")

// IsEmailValid checks if the email provided passes the required structure - thanks goes to https://golangnews.org/2020/06/validating-an-email-address/
func (c *Controller) IsEmailValid(e string) bool {

	if len(e) < 3 && len(e) > 254 {
		return false
	}

	if !emailRegex.MatchString(e) {
		return false
	}

	parts := strings.Split(e, "@")

	if len(parts) > 1 {
		subparts := strings.Split(parts[1], ".")

		if len(subparts) > 1 {
			mx, err := net.LookupMX(parts[1])

			if err != nil || len(mx) == 0 {
				return false
			}
		} else {
			return false
		}
	} else {
		return false
	}

	return true
}
