package controller

import (
	"net/http"

	sessions "github.com/Calidity/gin-sessions"
	"github.com/gin-gonic/gin"
	"github.com/gin-gonic/gin/binding"

	"gitlab.com/poerhiza/mailpipe/pkg/api/httputil"
	"gitlab.com/poerhiza/mailpipe/pkg/api/model"
)

// GetTransports godoc
// @Summary GetTransports will iterate over a users contacts and do a fuzzy match for contacts which match the supplied query
// @Description supplied with a query term, a users contacts are searched for a match - think of it as grep -i 'query*' contacts.txt
// @Tags transport
// @Accept  json
// @Produce  json
// @Param emailhash path string true "the contacts account to search" Format(string)
// @Success 200 {object} model.GetTransportsResponse
// @Failure 400 {object} httputil.HTTPError
// @Failure 404 {object} httputil.HTTPError
// @Failure 500 {object} httputil.HTTPError
// @Router /account/transport/ [get]
func (c *Controller) GetTransports(ctx *gin.Context) {
	session := sessions.Default(ctx)

	transports := model.GetTransports(session.Get("email").(string))

	ctx.JSON(http.StatusOK, transports)
}

// AddTransport godoc
// @Summary AddTransport will iterate over a users contacts and do a fuzzy match for contacts which match the supplied query
// @Description supplied with a query term, a users contacts are searched for a match - think of it as grep -i 'query*' contacts.txt
// @Tags transport
// @Accept  json
// @Produce  json
// @Param emailhash path string true "the contacts account to search" Format(string)
// @Success 200
// @Failure 400 {object} httputil.HTTPError
// @Failure 404 {object} httputil.HTTPError
// @Failure 500 {object} httputil.HTTPError
// @Router /account/transport/ [post]
func (c *Controller) AddTransport(ctx *gin.Context) {
	session := sessions.Default(ctx)

	var transport model.AddTransportRequest

	if err := ctx.ShouldBindBodyWith(&(transport.Transport), binding.JSON); err != nil {
		httputil.NewError(ctx, http.StatusBadRequest, err)
		return
	}

	if err := model.AddTransport(session.Get("email").(string), transport); err != nil {
		httputil.NewError(ctx, http.StatusInternalServerError, err)
		return
	}

	ctx.JSON(http.StatusOK, gin.H{
		"transport": transport,
	})
}

// RemoveTransport godoc
// @Summary RemoveTransport will iterate over a users contacts and do a fuzzy match for contacts which match the supplied query
// @Description supplied with a query term, a users contacts are searched for a match - think of it as grep -i 'query*' contacts.txt
// @Tags transport
// @Accept  json
// @Produce  json
// @Param emailhash path string true "the contacts account to search" Format(string)
// @Success 200
// @Failure 400 {object} httputil.HTTPError
// @Failure 404 {object} httputil.HTTPError
// @Failure 500 {object} httputil.HTTPError
// @Router /account/transport/delete [post]
func (c *Controller) RemoveTransport(ctx *gin.Context) {
	session := sessions.Default(ctx)

	var request model.RemoveTransportRequest

	if err := ctx.ShouldBindBodyWith(&request, binding.JSON); err != nil {
		httputil.NewError(ctx, http.StatusBadRequest, err)
		return
	}

	if err := model.RemoveTransport(session.Get("email").(string), request); err != nil {
		httputil.NewError(ctx, http.StatusInternalServerError, err)
		return
	}

	ctx.Status(http.StatusOK)
}
