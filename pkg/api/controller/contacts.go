package controller

import (
	"fmt"
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/gin-gonic/gin/binding"

	"gitlab.com/poerhiza/mailpipe/pkg/api/httputil"
	"gitlab.com/poerhiza/mailpipe/pkg/api/model"
	"gitlab.com/poerhiza/mailpipe/pkg/constants"
	"gitlab.com/poerhiza/mailpipe/pkg/logger"
	"gitlab.com/poerhiza/mailpipe/pkg/storage/redis"
)

// SearchContacts godoc
// @Summary SearchContacts will iterate over a users contacts and do a fuzzy match for contacts which match the supplied query
// @Description supplied with a query term, a users contacts are searched for a match - think of it as grep -i 'query*' contacts.txt
// @Tags contacts
// @Accept  json
// @Produce  json
// @Param emailhash body string true "the contacts account to search" Format(string)
// @Success 200 {object} model.ContactSearchResponse
// @Failure 400 {object} httputil.HTTPError
// @Failure 404 {object} httputil.HTTPError
// @Failure 500 {object} httputil.HTTPError
// @Router /account/contacts/{query} [post]
func (c *Controller) SearchContacts(ctx *gin.Context) {
	var contactSearch model.SearchContacts

	query := ctx.Param("query")

	if err := ctx.ShouldBindBodyWith(&contactSearch, binding.JSON); err != nil {
		httputil.NewError(ctx, http.StatusBadRequest, err)
		return
	}

	if !c.RegexContactsQuery.Match([]byte(query)) {
		httputil.NewError(ctx, http.StatusBadRequest, fmt.Errorf("validation of query failed - only 'a-zA-Z0-9 .@' are allowed"))
		return
	}

	matches := model.ContactSearch(ctx.GetString("email"), query)

	if len(matches) <= 0 && c.IsEmailValid(query) {
		matches = append(matches, model.Contact{Email: query, Name: query})
		err := redis.AddMetadataToEntry(constants.GetContactsKey(ctx.GetString("email")), query, query)

		if err != nil {
			logger.Log(err.Error())
		}
	}

	ctx.JSON(http.StatusOK, gin.H{
		"data": matches,
	})
}
