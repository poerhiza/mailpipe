package controller

import (
	"fmt"
	"net/http"
	"strings"

	"github.com/gin-gonic/gin"
	"github.com/gin-gonic/gin/binding"

	"gitlab.com/poerhiza/mailpipe/pkg/api/httputil"
	"gitlab.com/poerhiza/mailpipe/pkg/api/model"
	"gitlab.com/poerhiza/mailpipe/pkg/constants"
	"gitlab.com/poerhiza/mailpipe/pkg/logger"
	"gitlab.com/poerhiza/mailpipe/pkg/storage/redis"
)

// GetEveryBox godoc
// @Summary List an accounts box
// @Description get the box for an account
// @Tags box
// @Accept  json
// @Produce  json
// @Param emailhash path string true "the email box to check" Format(string)
// @Success 200 {object} model.Box
// @Failure 400 {object} httputil.HTTPError
// @Failure 404 {object} httputil.HTTPError
// @Failure 500 {object} httputil.HTTPError
// @Router /account/view/boxes [get]
func (c *Controller) GetEveryBox(ctx *gin.Context) {
	email := ctx.GetString("email")
	boxArray, err := redis.GetEveryBox(email)

	if err != nil {
		httputil.NewError(ctx, http.StatusNotFound, err)
		return
	}

	var boxes []string
	boxkeybase := constants.GetBoxKeyBase(email)

	for _, boxkey := range boxArray {
		item := strings.Split(boxkey, boxkeybase)

		if len(item) > 1 {
			boxes = append(boxes, item[1])
		}
	}

	ctx.JSON(http.StatusOK, gin.H{
		"boxes": boxes,
	})
}

// BoxValidation validate the box title
func (c *Controller) BoxValidation(ctx *gin.Context) {
	box := ctx.Param("box")

	if !c.RegexBox.Match([]byte(box)) {
		httputil.NewError(ctx, http.StatusNotFound, fmt.Errorf("box Failed Validation"))
		return
	}

	ctx.Set("box", box)

	ctx.Next()
}

// ListBox godoc
// @Summary List an accounts box
// @Description get the box for an account
// @Tags box
// @Accept  json
// @Produce  json
// @Param box path string true "the email box to check" Format(string)
// @Param emailhash path string true "the email box to check" Format(string)
// @Success 200 {object} model.Box
// @Failure 400 {object} httputil.HTTPError
// @Failure 404 {object} httputil.HTTPError
// @Failure 500 {object} httputil.HTTPError
// @Router /account/view/box/ [get]
func (c *Controller) ListBox(ctx *gin.Context) {
	email := ctx.GetString("email")
	box := ctx.GetString("box")
	date_start_in := ctx.Param("date_start")
	date_end_in := ctx.Param("date_end")

	var data []model.LinkedEmailBox

	boxArray, err := model.GetBox(email, box, date_start_in, date_end_in)

	if err != nil {
		//fmt.Println(err.(*errors.Error).ErrorStack())
		httputil.NewError(ctx, http.StatusNotFound, err)
		return
	}

	data = append(data, model.LinkedEmailBox{
		Email:     email,
		EmailHash: ctx.GetString("emailhash"),
		Messages:  boxArray,
	})

	if len(data) <= 0 {
		data = make([]model.LinkedEmailBox, 0)
	}

	ctx.JSON(http.StatusOK, data)
}

// GetBoxItem godoc
// @Summary List an accounts box
// @Description get the box for an account
// @Tags box
// @Accept  json
// @Produce  json
// @Param box path string true "the email box to check" Format(string)
// @Param emailhash path string true "the email box to check" Format(email)
// @Param id path string true "the email to retrieve from the box" Format(md5sum of the encoded string)
// @Success 200 {object} model.Email
// @Failure 400 {object} httputil.HTTPError
// @Failure 404 {object} httputil.HTTPError
// @Failure 500 {object} httputil.HTTPError
// @Router /account/view/box/{box}/message/{id} [get]
func (c *Controller) GetBoxItem(ctx *gin.Context) {
	email := ctx.GetString("email")
	box := ctx.GetString("box")
	id := ctx.Param("id")

	if id == "" || len(id) != 32 {
		logger.Log(fmt.Sprintf("param error: %s %s", email, id))
		httputil.NewError(ctx, http.StatusNotFound, fmt.Errorf("message not found"))
		return
	}

	MoveEmailFromUnreadToReadBox(box, email, id)

	boxItem, err := model.GetBoxItem(id)

	if err != nil {
		httputil.NewError(ctx, http.StatusNotFound, err)
		return
	}

	ctx.JSON(http.StatusOK, boxItem)
}

// GetBoxItemAttachments godoc
// @Summary List an accounts box items attachments
// @Description get the box item attachemnts for an account
// @Tags box
// @Accept  json
// @Produce  json
// @Param box path string true "the email box to check" Format(string)
// @Param emailhash path string true "the email box to check" Format(email)
// @Param id path string true "the email to retrieve from the box" Format(md5sum of the encoded string)
// @Success 200 {object} model.Email
// @Failure 400 {object} httputil.HTTPError
// @Failure 404 {object} httputil.HTTPError
// @Failure 500 {object} httputil.HTTPError
// @Router /account/view/box/{box}/message/{id}/download/attachments [get]
func (c *Controller) GetBoxItemAttachments(ctx *gin.Context) {
	id := ctx.Param("id")

	if id == "" || len(id) != 32 {
		logger.Log(fmt.Sprintf("param error: %s %s", ctx.GetString("email"), id))
		httputil.NewError(ctx, http.StatusNotFound, fmt.Errorf("message not found"))
		return
	}

	itemAttachments, err := redis.GetAttachments(id, "*")

	if err != nil {
		httputil.NewError(ctx, http.StatusNotFound, err)
		return
	}

	var attachments []string

	for _, item := range itemAttachments {
		fileItem := strings.Split(item, constants.Seperator)
		attachments = append(attachments, fileItem[len(fileItem)-1])
	}

	ctx.JSON(http.StatusOK, attachments)
}

// DownloadAttachment godoc
// @Summary List an accounts box items attachments
// @Description get the box item attachemnts for an account
// @Tags box
// @Accept  json
// @Produce  json
// @Param box path string true "the email box to check" Format(string)
// @Param emailhash path string true "the email box to check" Format(email)
// @Param id path string true "the email to retrieve from the box" Format(md5sum of the encoded string)
// @Param attachmenthash path string true "The md5sum of the attachment (the id)" Format(md5sum of the encoded string)
// @Success 200 {object} model.Email
// @Failure 400 {object} httputil.HTTPError
// @Failure 404 {object} httputil.HTTPError
// @Failure 500 {object} httputil.HTTPError
// @Router /account/view/box/{box}/message/{id}/download/attachment/{attachmenthash} [get]
func (c *Controller) DownloadAttachment(attachmentStoragePath string) gin.HandlerFunc {
	fn := func(ctx *gin.Context) {
		emailhash := ctx.Param("id")
		attachmenthash := ctx.Param("attachmenthash")

		if !c.RegexBox.Match([]byte(attachmenthash)) || !c.RegexBox.Match([]byte(emailhash)) {
			httputil.NewError(ctx, http.StatusNotFound, fmt.Errorf("email or Attachment hash failed validation"))
			return
		}

		if attachmenthash == "" || len(attachmenthash) != 32 || emailhash == "" || len(emailhash) != 32 {
			logger.Log(fmt.Sprintf("param error: %s %s %s", ctx.GetString("email"), emailhash, attachmenthash))
			httputil.NewError(ctx, http.StatusNotFound, fmt.Errorf("message not found"))
			return
		}

		var attachments []string

		attachments, err := redis.GetAttachments(emailhash, attachmenthash)

		if err != nil {
			logger.Log(err.Error())
			httputil.NewError(ctx, http.StatusNotFound, fmt.Errorf("message not found"))
			return
		}

		attachmentItem := strings.Split(attachments[0], constants.Seperator)
		attachmentPath := constants.GetAttachmentPath(attachmentStoragePath, emailhash, attachmenthash)

		ctx.Header("Content-Description", "File Transfer")
		ctx.Header("Content-Transfer-Encoding", "binary")
		ctx.Header("Content-Disposition", "attachment; filename="+attachmentItem[len(attachmentItem)-1])
		ctx.Header("Content-Type", "application/octet-stream")
		ctx.File(attachmentPath)
	}

	return gin.HandlerFunc(fn)
}

// UpdateBoxItems godoc
// @Summary Update items in an box
// @Description remove all of the provided message ids
// @Tags box
// @Accept  json
// @Produce  json
// @Param box path string true "the email box to check" Format(email)
// @Param emailhash path string true "the email box to check" Format(email)
// @Param messageids body array true "the list of ids to delete" Format(Array of message ids)
// @Success 200 {object} model.Email
// @Failure 400 {object} httputil.HTTPError
// @Failure 404 {object} httputil.HTTPError
// @Failure 500 {object} httputil.HTTPError
// @Router /account/view/box/{box}/message/ [post]
func (c *Controller) UpdateBoxItems(ctx *gin.Context) {

	email := ctx.GetString("email")
	box := ctx.GetString("box")

	var deleteMessagesRequest model.MessagesRequest

	if err := ctx.ShouldBindBodyWith(&deleteMessagesRequest, binding.JSON); err != nil {
		httputil.NewError(ctx, http.StatusBadRequest, err)
		return
	}

	if deleteMessagesRequest.Action == "purge" {
		emailBoxes, err := redis.GetEveryBox(email)

		if err != nil {
			httputil.NewError(ctx, http.StatusBadRequest, err)
			return
		}

		err = deleteMessagesRequest.Remove(true, emailBoxes)

		if err != nil {
			httputil.NewError(ctx, http.StatusNotFound, err)
			return
		}
	} else if deleteMessagesRequest.Action == "mark_read" && box == "unread" {

		for _, message := range deleteMessagesRequest.Messages {
			MoveEmailFromUnreadToReadBox(box, email, message.ID)
		}
	}

	ctx.JSON(http.StatusOK, "success")
}

func MoveEmailFromUnreadToReadBox(box string, email string, id string) {
	if box == "unread" {
		boxKeys, err := redis.GetBoxEntryByID(constants.GetBoxKey(email, box), id)

		if err != nil {
			logger.Log(err.Error())
		}

		// shuffle the email entry from unread box -> read box
		for _, scanMap := range boxKeys {
			for key, score := range scanMap {
				err = redis.AddBoxEntry(constants.GetBoxKey(email, constants.ReadBoxName), score, key)

				if err != nil {
					logger.Log(err.Error())
				}

				err := redis.RemoveBoxEntry(constants.GetBoxKey(email, box), key)

				if err != nil {
					logger.Log(err.Error())
				}
			}
		}
	}
}
