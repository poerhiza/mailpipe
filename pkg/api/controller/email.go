package controller

import (
	"fmt"
	"net/http"
	netMail "net/mail"
	"net/smtp"

	"gitlab.com/poerhiza/mailpipe/pkg/api/model"
	transportConfig "gitlab.com/poerhiza/mailpipe/pkg/config/transport"
	"gitlab.com/poerhiza/mailpipe/pkg/logger"
	"gitlab.com/poerhiza/mailpipe/pkg/storage/crypto"

	sessions "github.com/Calidity/gin-sessions"
	"github.com/gin-gonic/gin"
	"github.com/gin-gonic/gin/binding"
	"github.com/jhillyerd/enmime"

	"github.com/sendgrid/sendgrid-go"
	"github.com/sendgrid/sendgrid-go/helpers/mail"
)

// SendEmail godoc
// @Summary Send an email from the current account to another
// @Description supplied with a to,cc list send an email to them - supports 1..n attachments
// @Tags email
// @Accept  json
// @Produce  json
// @Param email body model.SendEmail true "The mail to be sent"
// @Param emailhash path string true "the account to send from" Format(string)
// @Success 200 {object} model.SendEmailResponse
// @Failure 400 {object} model.SendEmailResponse
// @Failure 404 {object} model.SendEmailResponse
// @Failure 500 {object} model.SendEmailResponse
// @Router /account/send/ [post]
func (c *Controller) SendEmail(ctx *gin.Context) {
	var sendEmail model.SendEmail

	emailFrom := ctx.GetString("email")

	if emailFrom == "" {
		ctx.JSON(http.StatusInternalServerError, gin.H{
			"data": "Failed to get your email address from session - try logging out and logging back in to create a new session",
		})
		return
	}

	if err := ctx.ShouldBindBodyWith(&sendEmail, binding.JSON); err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{"data": err.Error()})
		return
	}

	transport, err := model.TransportSearch(emailFrom, sendEmail.TransportID)

	if err != nil {
		ctx.JSON(http.StatusInternalServerError, gin.H{"data": err.Error()})
		return
	}

	var password string
	var salt string

	if transport.Status == transportConfig.TransportConfigStatusAccount {
		session := sessions.Default(ctx)
		password = fmt.Sprintf("%v", session.Get("user_tk"))
		salt = emailFrom
	}

	if transport.Status == transportConfig.TransportConfigStatusSiteWide {
		password = c.Password
		salt = c.Salt
	}

	switch transport.Data.GetType() {
	case transportConfig.TransportConfigTypeSendgrid:
		from := mail.NewEmail(" ", emailFrom)

		m := mail.NewV3Mail()
		m.SetFrom(from)

		m.Subject = sendEmail.Subject

		p := mail.NewPersonalization()

		var tos []*mail.Email
		for _, contact := range sendEmail.To {
			tos = append(tos, mail.NewEmail(contact.Name, contact.Email))
		}
		p.AddTos(tos...)

		var ccs []*mail.Email
		for _, contact := range sendEmail.CC {
			ccs = append(ccs, mail.NewEmail(contact.Name, contact.Email))
		}
		p.AddCCs(ccs...)

		var bccs []*mail.Email
		for _, contact := range sendEmail.BCC {
			bccs = append(bccs, mail.NewEmail(contact.Name, contact.Email))
		}
		p.AddBCCs(bccs...)
		p.Subject = sendEmail.Subject

		m.AddPersonalizations(p)
		content := mail.NewContent("text/plain", sendEmail.Text)
		m.AddContent(content)

		for _, attachment := range sendEmail.Attachments {
			a := mail.NewAttachment()
			a.SetContent(attachment.Content)

			attachmentType := attachment.Type

			if attachmentType == "" {
				attachmentType = "text/plain" // TODO: un-base64 encode the content and run mime over that to extract a type?
			}

			a.SetType(attachmentType)
			a.SetFilename(attachment.Filename)
			a.SetDisposition("attachment")
			a.SetContentID(attachment.Filename)
			m.AddAttachment(a)
		}

		privacyFooter, err := transport.Data.GetFooter()

		if err != nil {
			logger.Log(fmt.Sprintf("[!] Email failed: %v\n%s", sendEmail, err.Error()))
			ctx.JSON(http.StatusBadRequest, gin.H{"data": err.Error()})
			return
		}

		footerSetting := mail.NewFooterSetting()
		footerSetting.SetText(privacyFooter)
		footerSetting.SetEnable(true)
		footerSetting.SetHTML(fmt.Sprintf("<html><body>%s</body></html>", privacyFooter))

		mailSettings := mail.NewMailSettings()
		mailSettings.SetFooter(footerSetting)
		m.SetMailSettings(mailSettings)

		m.SetReplyTo(from)

		fmt.Printf("\n\ngoing to get API Key for %v %v %v\n\n", password, salt, transport.Status)

		apiKey, err := transport.Data.GetAPIKey(password, salt, transport.Status)

		if err != nil {
			logger.Log(fmt.Sprintf("[!] Email failed: %v\n%s", sendEmail, err.Error()))
			ctx.JSON(http.StatusBadRequest, gin.H{"data": err.Error()})
			return
		}

		url, err := transport.Data.GetURL()

		if err != nil {
			logger.Log(fmt.Sprintf("[!] Email failed: %v\n%s", sendEmail, err.Error()))
			ctx.JSON(http.StatusBadRequest, gin.H{"data": err.Error()})
			return
		}

		apiHost, apiEndpoint, err := transportConfig.ParseURL(url)

		fmt.Printf("%s %s %s %s\n", apiHost, apiEndpoint, apiKey, url)

		if err != nil {
			logger.Log(fmt.Sprintf("[!] Email failed: %v\n%s", sendEmail, err.Error()))
			ctx.JSON(http.StatusBadRequest, gin.H{"data": err.Error()})
			return
		}

		request := sendgrid.GetRequest(apiKey, apiEndpoint, apiHost)
		request.Method = "POST"
		request.Body = mail.GetRequestBody(m)
		response, err := sendgrid.API(request)

		if err != nil {
			logger.Log(fmt.Sprintf("[!] Email failed: %v\n%s", sendEmail, err.Error()))
			ctx.JSON(http.StatusBadRequest, gin.H{"data": err.Error()})
			return
		}

		logger.Log(fmt.Sprintf("[+] Email sent: %d - %s", response.StatusCode, response.Body))

		ctx.JSON(response.StatusCode, gin.H{
			"data": response.Body,
		})
		return

	case transportConfig.TransportConfigTypeSimple:
		var smtpUsername string
		var smtpPassword string

		smtpUsername, err := transport.Data.GetUsername(password, salt, transport.Status)

		if err != nil {
			logger.Log(fmt.Sprintf("[!] Email failed GetUsername: %v\n%s", sendEmail, err.Error()))
			ctx.JSON(http.StatusBadRequest, gin.H{"data": err.Error()})
			return
		}

		smtpPassword, err = transport.Data.GetPassword(password, salt, transport.Status)

		if err != nil {
			logger.Log(fmt.Sprintf("[!] Email failed GetPassword: %v\n%s", sendEmail, err.Error()))
			ctx.JSON(http.StatusBadRequest, gin.H{"data": err.Error()})
			return
		}

		smtpServer, err := transport.Data.GetHost()

		if err != nil {
			logger.Log(fmt.Sprintf("[!] Email failed GetHost: %v\n%s", sendEmail, err.Error()))
			ctx.JSON(http.StatusBadRequest, gin.H{"data": err.Error()})
			return
		}

		smtpPort, err := transport.Data.GetPort()

		if err != nil {
			logger.Log(fmt.Sprintf("[!] Email failed GetPort: %v\n%s", sendEmail, err.Error()))
			ctx.JSON(http.StatusBadRequest, gin.H{"data": err.Error()})
			return
		}

		privacyFooter, err := transport.Data.GetFooter()

		if err != nil {
			logger.Log(fmt.Sprintf("[!] Email failed GetFooter: %v\n%s", sendEmail, err.Error()))
			ctx.JSON(http.StatusBadRequest, gin.H{"data": err.Error()})
			return
		}

		// Create an SMTP Sender which relies on Go's built-in net/smtp package.  Advanced users
		// may provide their own Sender, or mock it in unit tests.
		smtpHost := fmt.Sprintf("%s:%d", smtpServer, smtpPort)
		smtpAuth := smtp.PlainAuth("", smtpUsername, smtpPassword, smtpServer)

		sender := enmime.NewSMTP(smtpHost, smtpAuth)

		var tos []netMail.Address
		for _, contact := range sendEmail.To {
			address := netMail.Address{
				Name:    contact.Name,
				Address: contact.Email,
			}
			tos = append(tos, address)
		}

		var ccs []netMail.Address
		for _, contact := range sendEmail.CC {
			address := netMail.Address{
				Name:    contact.Name,
				Address: contact.Email,
			}
			ccs = append(ccs, address)
		}

		var bccs []netMail.Address
		for _, contact := range sendEmail.BCC {
			address := netMail.Address{
				Name:    contact.Name,
				Address: contact.Email,
			}
			bccs = append(bccs, address)
		}

		master := enmime.Builder().
			From("", emailFrom).
			Subject(sendEmail.Subject).
			Text([]byte(fmt.Sprintf("%s\n\n%s", sendEmail.Text, privacyFooter))).
			HTML([]byte(fmt.Sprintf("<p>%s</p></br>%s", sendEmail.Text, privacyFooter))).
			ToAddrs(tos).
			CCAddrs(ccs).
			BCCAddrs(bccs).
			ReplyTo("", emailFrom)

		for _, attachment := range sendEmail.Attachments {
			attachmentType := attachment.Type

			if attachmentType == "" {
				attachmentType = "text/plain" // TODO: un-base64 encode the content and run mime over that to extract a type?
			}

			master = master.AddInline([]byte(attachment.Content), attachmentType, attachment.Filename, crypto.MD5HashString(attachment.Filename))
		}

		if master.Error() != nil {
			logger.Log(fmt.Sprintf("[!] Email failed formt error: %v\n", master.Error().Error()))
			ctx.JSON(http.StatusBadRequest, gin.H{"data": master.Error().Error()})
			return
		}

		err = master.SendWithReversePath(sender, emailFrom)

		if err != nil {
			logger.Log(fmt.Sprintf("[!] Email failed SendWithReversePath: %v\n%s", sendEmail, err.Error()))
			logger.Log(fmt.Sprintf("[!] Email failed SendWithReversePath Email: %v\n", master))
			ctx.JSON(http.StatusBadRequest, gin.H{"data": err.Error()})
			return
		}

		return
	default:
		logger.Log(fmt.Sprintf("[!] Email failed - unknown transport: %v\n%s", sendEmail, err.Error()))
		ctx.JSON(http.StatusInternalServerError, gin.H{"data": fmt.Errorf("unknown transport")})
		return
	}
}
