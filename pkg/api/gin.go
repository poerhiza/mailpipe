package api

import (
	"time"

	"net/http"

	iplimiter "github.com/Salvatore-Giordano/gin-redis-ip-limiter"
	"github.com/go-redis/redis"
	goRedis "github.com/redis/go-redis/v9"

	sessions "github.com/Calidity/gin-sessions"
	sessionsRedis "github.com/Calidity/gin-sessions/redis"
	helmet "github.com/danielkov/gin-helmet"

	gin "github.com/gin-gonic/gin"
	swaggerFiles "github.com/swaggo/files"
	ginSwagger "github.com/swaggo/gin-swagger"

	_ "gitlab.com/poerhiza/mailpipe/mkdocs/docs" // import the mailpipe swagger docs

	"gitlab.com/poerhiza/mailpipe/pkg/api/controller"
	"gitlab.com/poerhiza/mailpipe/pkg/logger"
)

func setup(mailpipeAPI *MailpipeAPI) *gin.Engine {

	if mailpipeAPI.Debug {
		gin.ForceConsoleColor()
		logger.Log("[D] Entering gin.setup")
	} else {
		gin.SetMode(gin.ReleaseMode)
	}

	c := new(controller.Controller)
	err := controller.Initialize(c)

	c.Debug = mailpipeAPI.Debug

	if err != nil {
		logger.Log(err.Error())
		return nil
	}

	r := gin.Default()

	store, err := sessionsRedis.NewRedisStore(goRedis.NewClient(&goRedis.Options{
		ClientName: "mailpipeAPI",
		Network:    "tcp",
		Addr:       mailpipeAPI.RedisServer,
	}), mailpipeAPI.SessionSecret[:])

	if err != nil {
		logger.Log(err.Error())
		return nil
	}

	if mailpipeAPI.Debug {
		r.Use(c.CORS)
		store.Options(sessions.Options{
			SameSite: http.SameSiteNoneMode,
			Path:     "/",
			Secure:   true,
			MaxAge:   60 * 60 * 24,
		})
	} else {
		store.Options(sessions.Options{
			SameSite: http.SameSiteStrictMode,
			Path:     "/",
			Secure:   true,
			MaxAge:   60 * 60 * 24,
		})
	}

	r.Use(sessions.Sessions("mailpipe", store))
	r.Use(helmet.Default())
	// opts := map[string]string{
	// 	"default-src": "'self'",
	// 	"img-src":     "*",
	// 	"media-src":   "'self'",
	// 	"script-src":  "'self'",
	// }
	// r.Use(helmet.ContentSecurityPolicy(opts, true))
	r.Use(iplimiter.NewRateLimiterMiddleware(redis.NewClient(&redis.Options{
		Addr:     mailpipeAPI.RedisServer,
		Password: "",
		DB:       1,
	}), "GinRateLimiterMiddleware", 200, 60*time.Second))

	r.Use(c.Static)

	v1 := r.Group("/api/v1")
	{
		//api/v1/authentication
		v1.POST("/authentication/:action", c.Authenticate)

		//api/v1/account
		account := v1.Group("/account")
		{
			account.Use(c.EmailHashValidation)

			//api/v1/account/link
			linkroot := account.Group("/link")
			{
				linkroot.POST("/get", c.GetLinkedAccounts)
				linkroot.POST("/list/:box/", c.ListLinkedBox)
				linkroot.POST("/request/list", c.ListLinkRequests)
				linkroot.POST("/request/link", c.RequestLink)
				linkroot.POST("/reply/:action", c.ReplyRequestLink)
				linkroot.POST("/remove/:perspective", c.DeleteLink)
			}

			//api/v1/account/contacts
			contacts := account.Group("/contacts") // EmailHashValidation
			{
				contacts.POST("/:query", c.SearchContacts)
			}

			//api/v1/account/transport
			transport := account.Group("/transport")
			{
				transport.POST("/get/", c.GetTransports)
				transport.POST("/add/", c.AddTransport)
				transport.POST("/delete/", c.RemoveTransport)
			}

			//api/v1/account/sendemail
			sendemail := account.Group("/sendemail") // EmailHashValidation
			{
				sendemail.POST("/", c.SendEmail)
			}

			//api/v1/account/view
			boxroot := account.Group("/view") // EmailHashValidation
			{
				//api/v1/account/view/boxes
				boxroot.POST("/boxes/", c.GetEveryBox)

				//api/v1/account/view/box/
				box := boxroot.Group("/box/:box")
				{
					box.Use(c.BoxValidation)
					box.POST("/", c.ListBox)
					// TODO: collapse ID + attachemnthash into post body params
					// TODO: consider https://github.com/gin-contrib/cache for these requests...
					box.POST("/message/:id", c.GetBoxItem)
					box.POST("/message/:id/download/attachments", c.GetBoxItemAttachments)
					box.POST("/message/:id/download/attachment/:attachmenthash", c.DownloadAttachment(mailpipeAPI.AttachmentStoragePath))
					// TODO: return a .eml file? box.GET("/message/:id/download", c.DownloadBoxItem)
					box.POST("/message/", c.UpdateBoxItems)
				}
			}
		}
	}

	if mailpipeAPI.Debug {
		r.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler))
	}

	return r
}
