package model

import "gitlab.com/poerhiza/mailpipe/pkg/constants"

// SendEmail example
type SendEmail struct {
	constants.APIRequest
	TransportID string            `json:"transportid"`
	To          []Contact         `json:"to"`
	CC          []Contact         `json:"cc"`
	BCC         []Contact         `json:"bcc"`
	From        string            `json:"from"`
	Subject     string            `json:"subject"`
	Text        string            `json:"text"`
	Attachments []emailAttachment `json:"attachments"`
}

type emailAttachment struct {
	Content  string `json:"content"`
	Type     string `json:"type"`
	Filename string `json:"filename"`
}

// SendEmailResponse is sent to a user upon completion of a sendemail request
type SendEmailResponse struct {
	Data string `json:"data"`
}
