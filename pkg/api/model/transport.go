package model

import (
	"encoding/json"
	"fmt"
	"strings"

	transportConfig "gitlab.com/poerhiza/mailpipe/pkg/config/transport"
	"gitlab.com/poerhiza/mailpipe/pkg/constants"
	"gitlab.com/poerhiza/mailpipe/pkg/logger"
	"gitlab.com/poerhiza/mailpipe/pkg/storage/redis"
)

// GetTransportsResponse is the struct holding a name and email for a contact
type GetTransportsResponse struct {
	Transports []transportConfig.TransportConfig `json:"transports"`
}

// AddTransportRequest request for adding an account transport
type AddTransportRequest struct {
	constants.APIRequest
	Transport transportConfig.TransportConfig `json:"transport"`
}

// RemoveTransportRequest request for removing an account transport
type RemoveTransportRequest struct {
	constants.APIRequest
	Title string `json:"title"`
}

// TransportSearch searches for a transport with id
func TransportSearch(email string, id string) (transportConfig.TransportConfig, error) {
	var configuredTransport transportConfig.TransportConfig

	matches, err := redis.ScanHashSet(constants.GetTransportKey(transportConfig.TransportConfigStatusSiteWide), fmt.Sprintf("%s|||*", id), 1)

	if err != nil {
		return configuredTransport, err
	}

	for k, v := range matches {
		transport := new(transportConfig.TransportConfig)

		err = transport.UnmarshalJSON([]byte(fmt.Sprintf("{\"data\": %s}", v)))

		if err != nil {
			return configuredTransport, err
		}
		transport.Title = k
		transport.Status = transportConfig.TransportConfigStatusSiteWide

		configuredTransport = *transport

		break // there should never be more than one...due the limit on redis.ScanHashSet
	}

	matches, err = redis.ScanHashSet(constants.GetTransportKey(email), fmt.Sprintf("%s|||*", id), 1)

	if err != nil {
		return configuredTransport, err
	}

	for k, v := range matches {
		transport := new(transportConfig.TransportConfig)

		err = transport.UnmarshalJSON([]byte(fmt.Sprintf("{\"data\": %s}", v)))

		if err != nil {
			return configuredTransport, err
		}
		transport.Title = k
		transport.Status = transportConfig.TransportConfigStatusAccount

		configuredTransport = *transport

		break // there should never be more than one...due the limit on redis.ScanHashSet
	}

	return configuredTransport, nil
}

// GetTransports searches an accounts (emailAddress) contacts for a match
func GetTransports(emailAddress string) GetTransportsResponse {
	var configuredTransport GetTransportsResponse

	transports, err := redis.GetSiteWideTransports()

	if err != nil {
		logger.Log(err.Error())
	}

	for k := range transports {

		transport := new(transportConfig.TransportConfig)
		titleParts := strings.Split(k, constants.Seperator) // TODO: eewww, leverage constants to break it back out?

		transport.ID = titleParts[0]
		transport.Title = titleParts[1]
		transport.Status = transportConfig.TransportConfigStatusSiteWide
		configuredTransport.Transports = append(configuredTransport.Transports, *transport)
	}

	transports, err = redis.GetUserTransports(emailAddress)

	if err != nil {
		logger.Log(err.Error())
	}

	for k := range transports {

		transport := new(transportConfig.TransportConfig)
		titleParts := strings.Split(k, constants.Seperator) // TODO: eewww, leverage constants to break it back out?

		transport.ID = titleParts[0]
		transport.Title = titleParts[1]
		transport.Status = transportConfig.TransportConfigStatusAccount
		configuredTransport.Transports = append(configuredTransport.Transports, *transport)
	}

	if len(configuredTransport.Transports) <= 0 {
		configuredTransport.Transports = make([]transportConfig.TransportConfig, 0)
	}

	return configuredTransport
}

// AddTransport adds the provided transport to the email accounts transports
func AddTransport(email string, transport AddTransportRequest) error {

	data, err := json.Marshal(transport.Transport.Data)

	if err != nil {
		return err
	}

	err = redis.AddUserTransport(email, transport.Transport.Title, string(data))

	if err != nil {
		logger.Log(err.Error())
	}

	return nil
}

// RemoveTransport searches an accounts (emailAddress) contacts for a match
func RemoveTransport(emailAddress string, request RemoveTransportRequest) error {

	err := redis.RemoveUserTransport(emailAddress, request.Title)

	if err != nil {
		return err
	}

	return nil
}
