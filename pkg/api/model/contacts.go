package model

import (
	"fmt"
	"regexp"

	"gitlab.com/poerhiza/mailpipe/pkg/constants"
	"gitlab.com/poerhiza/mailpipe/pkg/logger"
	"gitlab.com/poerhiza/mailpipe/pkg/storage/redis"
)

// SearchContacts contains a query for the contacts hashset
type SearchContacts struct {
	constants.APIRequest
}

// Contact is the struct holding a name and email for a contact
type Contact struct {
	Name  string `json:"name"`
	Email string `json:"email"`
}

// ContactSearchResponse is the struct holding a name and email for a contact
type ContactSearchResponse struct {
	Matches []Contact `json:"matches"`
}

// ContactSearch searches an accounts (emailAddress) contacts for a match
func ContactSearch(emailAddress string, query string) []Contact {
	var searchResult []Contact

	matches, err := redis.GetUserContacts(emailAddress)

	if err != nil {
		logger.Log(err.Error())
	}

	r, _ := regexp.Compile(fmt.Sprintf(".*%s.*", query))

	for k, v := range matches {
		if r.MatchString(k) || r.MatchString(v) {
			searchResult = append(searchResult, Contact{Email: k, Name: v})
		}
	}

	return searchResult
}
