package model

import (
	"log"
	"strings"

	"gitlab.com/poerhiza/mailpipe/pkg/constants"
	"gitlab.com/poerhiza/mailpipe/pkg/logger"
	"gitlab.com/poerhiza/mailpipe/pkg/storage/redis"
)

// BoxItem an item in a box
type BoxItem struct {
	ID       string `json:"id"`
	Email    string `json:"email"`
	Subject  string `json:"subject"`
	Datetime string `json:"datetime"`
}

// MessagesRequest an array of boxitem ids to remove
type MessagesRequest struct {
	constants.APIRequest
	Action   string    `json:"action"`
	Messages []BoxItem `json:"messages"`
}

// Remove the messageIds
func (dmr *MessagesRequest) Remove(removeEmail bool, boxes []string) error {

	for _, message := range dmr.Messages {

		if removeEmail { // remove primary hash for email
			err := redis.RemoveMetadataEntry(message.ID)

			if err != nil {
				log.Fatal(err)
				return err
			}
		}

		for _, boxKey := range boxes { // remove box entry for email
			boxKeys, err := redis.GetBoxEntryByID(boxKey, message.ID)

			if err != nil {
				logger.Log(err.Error())
			}

			for _, scanMap := range boxKeys {
				for messageKey := range scanMap {
					err := redis.RemoveBoxEntry(boxKey, messageKey)

					if err != nil {
						logger.Log(err.Error())
					}
				}
			}
		}
	}

	return nil
}

// Box is an organizational structure for an account much akin to 'folders' or 'directories' except the provide no nesting at this time
type Box struct {
	Email     Account   `json:"email"`
	EmailHash string    `json:"email_hash"`
	Messages  []BoxItem `json:"messages"`
}

// Email is an entry within a box item
type Email struct {
	HTML        string             `json:"html"`
	Text        string             `json:"text"`
	Errors      string             `json:"errors"`
	Attachments []EmailHeaderEntry `json:"attachments"`
	Headers     []EmailHeaderEntry `json:"headers"`
}

type EmailHeaderEntry struct {
	Key   string `json:"key"`
	Value string `json:"value"`
}

// Emails a list of email items
type Emails struct {
	List []Email `json:"emails"`
}

// Emails a list of email items
type LinkedEmailBox struct {
	Email     string    `json:"email"`
	EmailHash string    `json:"email_hash"`
	Messages  []BoxItem `json:"messages"`
}

// GetBox a listing of accounts
func GetBox(email string, box string, start_date_in string, end_date_in string) ([]BoxItem, error) {
	var emails []BoxItem
	var boxMappings []string

	if start_date_in != "" && end_date_in != "" {
		boxMapping, err := redis.GetBoxListingByRange(email, box, start_date_in, end_date_in)

		if err != nil {
			log.Fatal(err)
			return nil, err
		}

		boxMappings = boxMapping

	} else {
		boxMapping, err := redis.GetBoxListing(email, box)

		if err != nil {
			log.Fatal(err)
			return nil, err
		}

		boxMappings = boxMapping
	}

	for i := len(boxMappings) - 1; i >= 0; i-- { //reverse sort since inserts on sorted sets are FILO
		item := (boxMappings[i])

		boxItem := BoxItem{}
		entry := strings.Split(item, constants.Seperator)

		if len(entry) == 4 { // TODO: use reflection to get the number of fields vs. having a 'magic' number?...
			boxItem.Email = entry[0]
			boxItem.ID = entry[1]
			boxItem.Subject = entry[2]
			boxItem.Datetime = entry[3]
			emails = append(emails, boxItem)
		} // TODO: add logging for failures?
	}

	if len(emails) <= 0 {
		emails = make([]BoxItem, 0)
	}

	return emails, nil
}

// GetBoxItem expects a key to search redis for
func GetBoxItem(key string) (Email, error) {
	var email = Email{}

	emailItem, err := redis.GetBoxEntry(key)

	if err != nil {
		logger.Log(err.Error())
		return Email{}, err
	}

	for k, v := range emailItem {
		switch k {
		case "text":
			email.Text = v
		case "html":
			email.HTML = v
		case "errors":
			email.Errors = v
		default:

			if k != "" && v != "" {
				email.Headers = append(email.Headers, EmailHeaderEntry{Key: k, Value: v})
			}
		}
	}

	itemAttachments, _ := redis.GetAttachments(key, "*")

	for _, item := range itemAttachments {
		fileItem := strings.Split(item, constants.Seperator)
		email.Attachments = append(email.Attachments, EmailHeaderEntry{Key: fileItem[len(fileItem)-2], Value: fileItem[len(fileItem)-1]})
	}

	return email, nil
}

// GetBoxItemValue expects a key to search redis for
func GetBoxItemValue(boxItemKey string, itemKey string) (string, error) {

	value, err := redis.GetBoxEntryKey(boxItemKey, itemKey)

	if err != nil {
		logger.Log(err.Error())
		return "", err
	}

	return value, nil
}
