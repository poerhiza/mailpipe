package model

import (
	"fmt"

	"golang.org/x/crypto/bcrypt"

	"gitlab.com/poerhiza/mailpipe/pkg/constants"
	"gitlab.com/poerhiza/mailpipe/pkg/logger"
	"gitlab.com/poerhiza/mailpipe/pkg/storage/redis"
)

// Account example
type Account struct {
	Email       string `json:"email,omitempty" example:"email@domain.tld"`
	Password    string `json:"password,omitempty" example:"5f4dcc3b5aa765d61d8327deb882cf99"`
	TK          string `json:"tk,omitempty" example:"5f4dcc3b5aa765d61d8327deb882cf99"`
	NewPassword string `json:"newpassword,omitempty" example:"5f4dcc3b5aa765d61d8327deb882cf99"`
	Unread      int    `json:"unread,omitempty" example:"4"`
}

// AccountLinkRequest
type AccountLinkRequest struct {
	constants.APIRequest
	Email string `json:"email" example:"email@domain.tld"`
}

// Account example
type AccountLink struct {
	Hash   string `json:"account_hash" example:"email@domain.tld"`
	Unread int    `json:"unread" example:"4"`
}

// Accounts a list of accounts
type Accounts struct {
	List []Account `json:"accounts"`
}

// Authenticate a user to an email address
func Authenticate(account Account) bool {

	hashedPassword, err := redis.GetAccountPassword(account.Email)

	if err != nil {
		logger.Log(err.Error())
		return false
	}

	if hashedPassword == "" {
		logger.Log(fmt.Errorf("[!] empty password detected").Error())
		return false
	}

	return nil == bcrypt.CompareHashAndPassword([]byte(hashedPassword), []byte(account.Password))
}

// ChangePassword when supplied with an Account model which has a password and newpassword field set - attempt to update the password
func ChangePassword(account Account) error {

	if Authenticate(account) {

		hashedPassword, err := bcrypt.GenerateFromPassword([]byte(account.NewPassword[0:72]), bcrypt.DefaultCost)

		if err != nil {
			return err
		}

		account.NewPassword = string(hashedPassword)

		return redis.SetAccountPassword(account.Email, account.NewPassword)
	}

	return fmt.Errorf("current password is incorrect")
}
