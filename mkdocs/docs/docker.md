# One-time-setup

```bash
docker network create --attachable --subnet 172.16.242.0/24 mailpipe
```

## Quick Start

0. `git clone git@gitlab.com:poerhiza/mailpipe.git && cd mailpipe`

1. Update the configuration files found under ./scripts/docker/ to match what you desire (or update the docker-compose.yml with paths to your configuration files)

1. Run `docker-compose up -d`

1. Visit [The GUI](https://172.16.242.10:8443/)

1. Use the default username/password of **mailpipe@changeme.tld**

**_The default password will only work if you didn't change the agent.json file - any mailboxes initial password is it's address_**

### Binaries

If using the `docker-compose-dev.yml` file, you can simply copy the built binaries out.

```bash
docker-compose -f docker-compose-dev.yml build builder

docker cp mailpipe_builder:/build/dist/temp/mailpipe-linux_amd64 /tmp
docker cp mailpipe_builder:/build/dist/temp/mailpipe-api-linux_amd64 /tmp
docker cp mailpipe_builder:/build/dist/temp/mailpipe-config-linux_amd64 /tmp
docker cp mailpipe_builder:/build/dist/temp/mailpipe-pipemail-linux_amd64 /tmp
```

### Developers

The recomended method by which mailpipe is developed is docker - because who wants to install node_modules / golang modules and bloat their host...!

#### Docker / docker-compose

Then run:

```bash
docker-compose -f docker-compose-dev.yml up
# once built and running, go visit https://172.16.242.10:8443
```

#### Update packages

```bash
docker-compose -f docker-compose-dev.yml up
make terminal_api
apk add make git go
make update
```

#### Scripts

The scripts folder has a number of 'helpers' - if doing development and you want a lot of fake emails - review `dev_faker_sendmail.py`.
