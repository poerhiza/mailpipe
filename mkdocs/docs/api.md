# API

A Swagger documented API is provided, see the provided [swagger.yaml](https://gitlab.com/poerhiza/mailpipe/-/blob/master/mkdocs/docs/swagger.yaml), [swagger.json](https://gitlab.com/poerhiza/mailpipe/-/blob/master/mkdocs/docs/swagger.json), or start up the API cmd in debug mode to launch the integrated swagger document server.

By default, when the **-debug** flag is present, the mailpipe-api will host the [Swagger Documentation](https://localhost:8443/swagger/index.html)

**NOTE**: _The **-debug** flag must be passed for CORS to be enabled or any GUI development will be most tiresome..._ By default - CORS is disabled in production (when the -debug flag isn't present).
