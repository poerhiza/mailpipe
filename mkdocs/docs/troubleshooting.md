## Nothing is working!

### Mailpipe API

By default, when the **-debug** flag is present, the mailpipe-api will host the [Swagger Documentation](https://localhost:8443/swagger/index.html)

**NOTE**: *The **-debug** flag must be passed for CORS to be enabled or any GUI development will be most tiresome...* By default - CORS is disabled in production (when the -debug flag isn't present).

### Mailpipe Mail Agent

By default, when the **-debug** flag is present, the mailpipe will print debug messages which might help diagnoise transport issues.