## Requirements

### Redis

Make sure redis is setup, enabled, and running (this is the only storage interface currently supported)

### Create a key-pair (if using TLS/STARTTLS)

```bash
openssl req -newkey rsa:4096 -nodes -keyout private_key.pem -x509 -days 9001 -out public_certificate.pem
```

I utilize [step-ca](https://smallstep.com/docs/step-ca)

### Get the binary

[Download](https://gitlab.com/poerhiza/mailpipe/-/releases) or build the mailpipe binaries.

If you've Golang and make - you can build it yourself.

```bash
# The following will build the binaries for Linux

make linux
```

### Configuration

Mailpipe requires two configuration files - one for the API/GUI binary and one for the mail agent.

**/etc/mailpipe/api.json** Create a configuration file - an example is as follows:

```json
{
  "common": {
    "certificate": "/etc/mailpipe/certificate.pem",
    "key": "/etc/mailpipe/key.pem",
    "certificate_authority": "/etc/mailpipe/ca.pem",
    "mtls": false,
    "host": "localhost",
    "port": 8443,
    "attachment_storage_path": "/opt/mailpipe/data/"
  },
  "transports": [
    {
      "title": "Sendgrid Web API",
      "data": {
        "type": "sendgrid",
        "url": "https://api.sendgrid.com/v3/mail/send",
        "apikey": "use mailpipe_config to create this value",
        "footer": "\n\nThis e-mail and any attachments it may contain are confidential and privileged information. If you are not the intended recipient, please notify the sender immediately by return e-mail, delete this e-mail and destroy any copies. Any dissemination or use of this information by any other than the intended recipient is unauthorized and may be illegal."
      }
    },
    {
      "title": "mailpipe@changeme.tld",
      "data": {
        "type": "simple",
        "host": "smtp.gmail.com",
        "port": 587,
        "username": "use mailpipe_config to create this value",
        "password": "use mailpipe_config to create this value",
        "footer": "\n\nThis e-mail and any attachments it may contain are confidential and privileged information. If you are not the intended recipient, please notify the sender immediately by return e-mail, delete this e-mail and destroy any copies. Any dissemination or use of this information by any other than the intended recipient is unauthorized and may be illegal."
      }
    }
  ],
  "redis": {
    "protocol": "tcp",
    "address": "127.0.0.1:6379",
    "poolsize": 10
  }
}
```

**/etc/mailpipe/smtp.json** Create a configuration file - an example is as follows:

```json
{
  "common": {
    "certificate": "/etc/mailpipe/certificate.pem",
    "key": "/etc/mailpipe/key.pem",
    "certificate_authority": "/etc/mailpipe/ca.pem",
    "mtls": false,
    "host": "localhost",
    "port": -1,
    "attachment_storage_path": "/opt/mailpipe/data/"
  },
  "protocol": "smtp",
  "attachment_max": 13,
  "filters": [
    {
      "domain": "changeme.tld",
      "mapping": [
        {
          "handle": "mailpipe",
          "boxes": [
            {
              "title": "spam",
              "rules": ["header:recived:.*223\\.223\\.218\\.9.*"],
              "expire": "168h"
            }
          ]
        }
      ]
    }
  ],
  "redis": {
    "protocol": "tcp",
    "address": "127.0.0.1:6379",
    "poolsize": 10
  }
}
```

Example command line invocation for both the API/GUI and the mail agent:

**Mail Agent**
```bash
./mailpipe -v -config ./config/mailbox.json
```

**GUI**
```bash
./mailpipe-api -v -config ./config/api.json
```

### Setup as a service

#### Fedora SystemD Setup

##### Install the built binaries
**/usr/local/bin/**

##### Create a mailpipe user

```bash
sudo useradd -d /opt/mailpipe -r -U -s /sbin/nologin mailpipe
```

##### Create crypto stuff (I use step-ca)

```bash
mkdir /etc/mailpipe
step ca init --name "Local CA" --provisioner admin --dns localhost --address ":443"
# step ca certificate --offline mailpipe.local certificate.pem key.pem
step ca certificate --san 012.345.678.910 --san domain.com domain.com /etc/mailpipe/certificate.pem /etc/mailpipe/key.pem --not-after=8760h --crv=P-521  -kty=EC
step ca root /etc/mailpipe/ca.pem
```

##### Install, start, and enable redis on boot

```bash
dnf install redis
systemctl enable redis
service redis start
```

##### Make sure the firewall is happy

```bash
firewall-cmd --add-port=25/tcp --permanent # smtp
firewall-cmd --add-port=465/tcp --permanent # tls
firewall-cmd --add-port=587/tcp --permanent # starttls
firewall-cmd --add-port=443/tcp --permanent # api
firewall-cmd --reload
```

##### Create the Mailpipe API ENV file

```bash
cat >> /etc/sysconfig/mailpipe_api <<EOF
MAILPIPE_API_CONFIG_PASSWORD='keepitsecretkeepitsafe'
MAILPIPE_API_CONFIG_SALT='daf3ae813ad102e1014c6a5cb9b3a7fb'
EOF
```

##### Create some service files

###### Systemd

**Mailpipe (Agent)**

```bash
[Unit]
Description=Mailpipe - SMTP
ConditionPathExists=/usr/local/bin/mailpipe
After=network.target redis.service
BindsTo=redis.service

[Service]
AmbientCapabilities=CAP_NET_BIND_SERVICE
Type=simple
User=mailpipe
LimitNOFILE=1024

Restart=on-failure
RestartSec=10

WorkingDirectory=/opt/mailpipe
ExecStart=/usr/local/bin/mailpipe -config /etc/mailpipe/smtp.json

# Log errors and stdout to the journal
PermissionsStartOnly=true
StandardOutput=journal
StandardError=journal
SyslogIdentifier=mailpipe-smtp

[Install]
WantedBy=multi-user.target
```

**Mailpipe (API)**

```bash
[Unit]
Description=Mailpipe - API
ConditionPathExists=/usr/local/bin/mailpipe-api
After=network.target redis.service
BindsTo=redis.service

[Service]
AmbientCapabilities=CAP_NET_BIND_SERVICE
Type=simple
User=mailpipe
LimitNOFILE=1024

Restart=on-failure
RestartSec=10

EnvironmentFile=/etc/sysconfig/mailpipe_api

WorkingDirectory=/opt/mailpipe
ExecStart=/usr/local/bin/mailpipe-api -config /etc/mailpipe/api.json

# Log errors and stdout to the journal
PermissionsStartOnly=true
StandardOutput=journal
StandardError=journal
SyslogIdentifier=mailpipe-api

# Some distributions may not support these hardening directives. If you cannot start the service due
# to an unknown option, comment out the ones not supported by your version of systemd.
# See https://www.freedesktop.org/software/systemd/man/systemd.exec.html
# for details
ProtectSystem=strict
ProtectHome=true
InaccessiblePaths=-/lost+found
NoExecPaths=/
ExecPaths=/usr/local/bin/mailpipe-api
PrivateTmp=true
PrivateDevices=true
ProtectHostname=true
ProtectClock=true
ProtectKernelTunables=true
ProtectKernelModules=true
ProtectKernelLogs=true
ProtectControlGroups=true
LockPersonality=true
RestrictAddressFamilies=AF_INET AF_INET6
NoNewPrivileges=true

[Install]
WantedBy=multi-user.target
```

##### Make sure SELinux is happy

```bash
restorecon -v /usr/local/bin/mailpipe*
restorecon -v /etc/sysconfig/mailpipe*
restorecon -Rv /opt/mailpipe
```

##### Move the service files to the correct location and enable them on boot

```bash 
mv *.service /etc/systemd/system/
systemctl daemon-reload
systemctl enable mailpipe-api
systemctl enable mailpipe-smtp
```

##### Start the services

```bash
service mailpipe-api start
service mailpipe-smtp start
```

##### Those pesky spammers

The firewall scripts, if you want to leverage them to 'block spammers', can be used.

**firewalld**

```bash
#!/usr/bin/env bash
#
# Setup as a cronjob - run "vim /etc/crontab"
#
# Then Put the following in /etc/crontab: 
#
#  0  5  *  *  * root       /root/update_firewalld_blocklist.sh
#
# Then place this script at /root/update_firewalld_blocklist.sh
#
# This script will then run every day at 0500
#

/usr/bin/redis-cli --raw HKEYS sender_blocklist | egrep -v '127.0.0.1' > /tmp/blocklist
for ip in `cat /tmp/blocklist | sort | uniq`; do /usr/bin/firewall-cmd --permanent --add-rich-rule="rule family='ipv4' source address='$ip' reject"; echo "Blocked $ip"; done
rm -f /tmp/blocklist

/usr/bin/firewall-cmd --reload
```

**ipfw**

```bash
#!/usr/bin/env bash

redis-cli --raw HKEYS sender_blocklist | egrep -v '127.0.0.1' > /tmp/blocklist
cat /etc/firewall.blocklist >> /tmp/blocklist
cat /tmp/blocklist | sort | uniq > /etc/firewall.blocklist
rm -f /tmp/blocklist

service ipfw restart
```

##### Other

Link all accounts under a specific account (that specific account can then access all the boxes for every other account).

```bash
#!/bin/bash

EMAILS=`echo 'hkeys emails' | redis-cli -e --raw`
EMAILS_MENU=`echo -ne "${EMAILS}\ndone"`

PS3='Link all accounts under email #) '
emails_menu=($EMAILS_MENU)
emails=($EMAILS)
select primary in "${emails_menu[@]}"; do
    case $primary in
	"done")
	    echo "User requested exit"
	    exit
	    ;;
         *)
        echo "option $primary"
        if [[ " ${emails[*]} " =~ " ${primary} " ]]; then
          echo "Using $primary as your primary email to link all other accounts with"
          for e in $EMAILS; do
            if [[ "${primary}" == "${e}" ]]; then
              continue
            fi
            k=`echo "hget emails ${e}" | redis-cli`
            a=`echo "${e}${k}" | mailpipe-config -hash | tr -d '\n' | cut -f3 -d:`
            echo "HMSET ${primary}|||account_links ${e} ${a}" | redis-cli -e --raw
          done
        else
          echo "Invalid option: ${REPLY}"
        fi
        ;;
    esac
done
```
