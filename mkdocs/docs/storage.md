# Storage

Emails, contacts, attachments, and spammers are all stored post processing. Currently - only Redis is used for storage of data.

## Redis

Redis is leveraged for storage of emails and their related metadata.

The keys are organized like so:

* Gin sessions are stored under session_<hash> (NA)
* 'Unread' are by handle and stored at the **boxkey** "<handle>|||unread|||<boxname>"  (zset)
 * Each entry in boxes has an **emailentrykey** - think of the **emailentrykey** as a pointer since it's really the **emailkey** that contains the email information **emailentrykey** format is: "<handle>@<domain><emailkey><subject><datetime>" (hash)
* 'Emails' are parsed and then stored under the **emailkey** as K/V items (hash)
* Username/passwords are stored under "emails" key as email/password (hash)
* Attachments for an email are stored under a number of keys in the format of "<emailkey>attachments<attachmenthash><filename>" (set)

A diagram of that mentioned above can be seen below:

![Mailpipe Redis Storage Schema](./images/mailpipe_redis_storage_schema.svg)