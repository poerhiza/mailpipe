export GO111MODULE=on

VERSION=$(shell cat pkg/mailpipe/mailpipe.go |grep "const Version ="|cut -d"\"" -f2)

BUILD=$(shell git rev-parse HEAD)
BASEDIR=./dist
DIR=${BASEDIR}/temp

LDFLAGS=-ldflags "-s -w -X main.build=${BUILD} -X gitlab.com/poerhiza/mailpipe/pkg.BuildVersion=${VERSION} -buildid=${BUILD}"
GCFLAGS=-gcflags=all=-trimpath=$(shell echo ${HOME})
ASMFLAGS=-asmflags=all=-trimpath=$(shell echo ${HOME})

GOFILES=`go list -buildvcs=false ./...`
GOFILESNOTEST=`go list -buildvcs=false ./... | grep -v test`

# Make Directory to store executables
$(shell mkdir -p ${DIR})

all: linux freebsd
# goreleaser build --config .goreleaser.yml --rm-dist --skip-validate

freebsd: lint docs
	@env CGO_ENABLED=0 GOOS=freebsd GOARCH=amd64 go build -trimpath ${LDFLAGS} ${GCFLAGS} ${ASMFLAGS} -o ${DIR}/mailpipe-freebsd_amd64 cmd/mailpipe/main.go
	@env CGO_ENABLED=0 GOOS=freebsd GOARCH=amd64 go build -trimpath ${LDFLAGS} ${GCFLAGS} ${ASMFLAGS} -o ${DIR}/mailpipe-api-freebsd_amd64 cmd/api/main.go
	@env CGO_ENABLED=0 GOOS=freebsd GOARCH=amd64 go build -trimpath ${LDFLAGS} ${GCFLAGS} ${ASMFLAGS} -o ${DIR}/mailpipe-config-freebsd_amd64 cmd/config/main.go
	@env CGO_ENABLED=0 GOOS=freebsd GOARCH=amd64 go build -trimpath ${LDFLAGS} ${GCFLAGS} ${ASMFLAGS} -o ${DIR}/mailpipe-pipemail-freebsd_amd64 cmd/pipemail/main.go
	@env CGO_ENABLED=0 GOOS=freebsd GOARCH=amd64 go build -trimpath ${LDFLAGS} ${GCFLAGS} ${ASMFLAGS} -o ${DIR}/mailpipe-pipereplay-freebsd_amd64 cmd/pipereplay/main.go

linux: lint docs
	@env CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -trimpath ${LDFLAGS} ${GCFLAGS} ${ASMFLAGS} -o ${DIR}/mailpipe-linux_amd64 cmd/mailpipe/main.go
	@env CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -trimpath ${LDFLAGS} ${GCFLAGS} ${ASMFLAGS} -o ${DIR}/mailpipe-api-linux_amd64 cmd/api/main.go
	@env CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -trimpath ${LDFLAGS} ${GCFLAGS} ${ASMFLAGS} -o ${DIR}/mailpipe-config-linux_amd64 cmd/config/main.go
	@env CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -trimpath ${LDFLAGS} ${GCFLAGS} ${ASMFLAGS} -o ${DIR}/mailpipe-pipemail-linux_amd64 cmd/pipemail/main.go
	@env CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -trimpath ${LDFLAGS} ${GCFLAGS} ${ASMFLAGS} -o ${DIR}/mailpipe-pipereplay-linux_amd64 cmd/pipereplay/main.go

docs:
	@rm -f mkdocs/docs/docs.go mkdocs/docs/swagger.json mkdocs/docs/swagger.yaml
	@swag init --parseDependency --parseInternal  --output mkdocs/docs/ -g cmd/api/main.go

submodules:
	@git submodule update --init --recursive

tidy:
	@go mod tidy

update: tidy
	@go get -v -d ./...
	@go get -u all

dep: ## Get the dependencies
	@git config --global url."git@github.com:".insteadOf "https://github.com/"
	# @go install github.com/goreleaser/goreleaser
	@go install github.com/boumenot/gocover-cobertura@latest
	@go install github.com/swaggo/swag/cmd/swag@v1.7.8
	@go install github.com/securego/gosec/v2/cmd/gosec@latest

lint: ## Lint the files
	@env CGO_ENABLED=0 go fmt ${GOFILES}
	@env CGO_ENABLED=0 go vet ${GOFILESNOTEST}

security:
	@gosec -tests ./...

test: lint security ## Run unit tests
	echo ""
	#@env CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go test -coverprofile=${DIR}/coverage.out -short ${GOFILESNOTEST} > ${DIR}/coverage.stdout
	#@env CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go tool cover -html=${DIR}/coverage.out -o ${DIR}/coverage.html
	#@gocover-cobertura < ${DIR}/coverage.out > ${DIR}/coverage.xml
	#@cat ${DIR}/coverage.stdout | grep 'pkg/mailpipe'

release:
	@goreleaser release --config .github/goreleaser.yml

clean:
	@rm -rf ${BASEDIR}

terminal_api:
	@echo 'go run ./cmd/api/main.go -v -debug -config /etc/mailpipe/api.json'
	@docker exec -it mailpipe_api /bin/ash

terminal_agent:
	@echo 'go run cmd/mailpipe/main.go -v -config /etc/mailpipe/agent.json'
	@docker exec -it mailpipe_agent /bin/ash

terminal_redis:
	@echo 'redis-cli KEYS *'
	@docker exec -it mailpipe_redis /bin/bash

.PHONY: all freebsd linux docs submodule tidy update dep lint security test release clean terminal
