# mailpipe

[![pipeline status](https://gitlab.com/poerhiza/mailpipe/badges/master/pipeline.svg)](https://gitlab.com/poerhiza/mailpipe/-/commits/master) | [![coverage report](https://gitlab.com/poerhiza/mailpipe/badges/master/coverage.svg)](https://gitlab.com/poerhiza/mailpipe/-/commits/master)

Piping E-Mail around like it's the 90's and the only OS is UNIX!

More information found [here](https://poerhiza.gitlab.io/mailpipe/).


```bash
BUILDKIT_PROGRESS=plain docker compose -f docker-compose-dev.yml build
BUILDKIT_PROGRESS=plain docker compose -f docker-compose-dev.yml up
```
